# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Create Roles
['superadmin', 'admin', 'admin_cleaner', 'area-manager', 'supervisor', 'cleaner', 'secundary-cleaner', 'contractor', 'staff', 'dispatcher', 'client', 'user'].each do |role|
  Role.find_or_create_by({name: role})
end

# Create Clients
# 5.times do
#   Client.create({
#     name: Faker::Company.name,
#     address: Faker::Address.city,
#     address2: Faker::Address.city,
#     state: 'State x',
#     zipcode: 'ZipCode x',
#     city: 'City X',
#     phone: Faker::PhoneNumber.phone_number,
#     email: Faker::Internet.email,
#     status: 1
#   })
# end

# Create Default and first Superdmin User
User.create(
  username: "superadmin",
  email: "superadmin@vericleanservices.com",
  first_name: "Superadmin",
  last_name: "Vericlean Services",
  password: "12345678",
  password_confirmation: "12345678",
  role_id: 1,
  client_id: 0,
  phone: "123456789",
  address: "San Antonio, TX",
  status: 1)

# Create Cleaners
1.times do
  User.create({
    username: "cleaner",
    email: "cleaner@vericleanservices.com",
    first_name: "Vericlean Services",
    last_name: "Test",
    password: "12345678",
    password_confirmation: "12345678",
    role_id: 4,
    client_id: 1,
    phone: "123456789",
    address: "San Antonio, TX",
    status: 1
  })
end

# Create ServiceTypes
servicetypes = ["Janitorial"]
servicetypes.each do |item|
  ServiceType.create({
    name: item,
    min_time: '40',
    max_time: '60'
  })
end
