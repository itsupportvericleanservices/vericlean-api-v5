class AddIndirectClients < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :indirect, :integer, :limit => 1, after: 'id', default: 0
    add_column :clients, :indirect_id, :integer, :limit => 8, after: 'indirect'
  end
end
