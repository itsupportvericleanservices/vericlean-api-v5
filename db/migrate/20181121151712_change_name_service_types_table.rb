class ChangeNameServiceTypesTable < ActiveRecord::Migration[5.2]
  def change
    rename_table :servicetypes, :service_types
  end
end
