class AddColumnToClients < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :deleted, :boolean, default: 0
  end
end
