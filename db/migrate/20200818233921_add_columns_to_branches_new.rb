class AddColumnsToBranchesNew < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :weekly_payout, :decimal, precision: 20, scale: 2
    add_column :branches, :company_payroll, :string
    add_column :branches, :type_of_contract, :string
  end
end
