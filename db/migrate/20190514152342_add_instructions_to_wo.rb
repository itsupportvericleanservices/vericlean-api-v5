class AddInstructionsToWo < ActiveRecord::Migration[5.2]
  def change
  	add_column :work_orders, :instruction, :string, after: :task_description
  end
end
