class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.references :route_schedule
      t.references :branch
      t.datetime :started_at
      t.datetime :finished_at
      t.boolean :status

      t.timestamps
    end
  end
end
