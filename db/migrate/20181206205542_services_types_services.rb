class ServicesTypesServices < ActiveRecord::Migration[5.2]
  def change
    create_table :service_types_services do |t|
      t.references :service
      t.references :service_type

      t.timestamps
    end
  end
end
