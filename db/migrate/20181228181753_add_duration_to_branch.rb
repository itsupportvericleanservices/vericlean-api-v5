class AddDurationToBranch < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :duration_min, :integer, after: 'max_time'
    add_column :branches, :duration_max, :integer, after: 'duration_min'
  end
end
