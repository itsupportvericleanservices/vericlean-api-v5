class BranchesServicetypes < ActiveRecord::Migration[5.2]
  def change
    create_table :branches_servicetypes do |t|
      t.references :branch
      t.references :servicetype
      t.integer :quantity

      t.timestamps
    end
  end
end
