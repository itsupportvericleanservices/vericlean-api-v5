class AddDeletedToBuildingTypes < ActiveRecord::Migration[5.2]
  def change
    add_column :building_types, :deleted, :boolean, after: :name, default: 0
  end
end
