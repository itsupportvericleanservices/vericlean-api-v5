class AddFieldsToRouteHistories < ActiveRecord::Migration[5.2]
  def change
  	add_reference :route_histories, :cleaner, after: :user_id
    add_reference :route_histories, :supervisor, after: :cleaner_id
    add_reference :route_histories, :area_manager, after: :supervisor_id
  end
end
