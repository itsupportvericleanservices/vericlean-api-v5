class AddReferenceOfBuildingTypeToBranches < ActiveRecord::Migration[5.2]
  def change
    add_reference :branches, :building_type, after: :supervisor_id
  end
end
