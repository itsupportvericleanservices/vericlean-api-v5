class RenameSiteContactEmailWo < ActiveRecord::Migration[5.2]
  def change
  	rename_column :work_orders, :site_contact_email, :request_contact_email
  end
end
