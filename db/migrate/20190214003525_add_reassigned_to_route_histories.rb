class AddReassignedToRouteHistories < ActiveRecord::Migration[5.2]
  def change
  	add_column :route_histories, :assigned_id, :integer, limit: 5, after: :user_id

    add_index :route_histories, :assigned_id
  end
end
