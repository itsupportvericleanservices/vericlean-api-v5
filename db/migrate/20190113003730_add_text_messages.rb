class AddTextMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :text_messages do |t|
      t.references :user
      t.integer :sent_to
      t.string :to
      t.string :from
      t.text :body
      t.string :message_type
      t.string :status

      t.timestamps
    end
  end
end
