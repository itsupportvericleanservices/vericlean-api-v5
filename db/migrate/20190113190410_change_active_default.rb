class ChangeActiveDefault < ActiveRecord::Migration[5.2]
  def change
    change_column :branches, :active, :integer, limit: 1, default: 1, after: :supervisor_id
  end
end
