class CreateJoinTableBranchesExtraSupervisors < ActiveRecord::Migration[5.2]
  def change
    create_join_table :branches, :users, table_name: :branches_extra_supervisors do |t|
      t.index [:branch_id, :user_id]
      t.index [:user_id, :branch_id]
    end
  end
end
