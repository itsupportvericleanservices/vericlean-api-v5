class AddSignatureToWorkOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :work_orders, :signature, :text
  end
end
