class RenameManytoManyServiceType < ActiveRecord::Migration[5.2]
  def change
    rename_column :branches_service_types, :servicetype_id, :service_type_id
  end
end
