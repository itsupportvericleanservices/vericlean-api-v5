class ChangeInstructionToWo < ActiveRecord::Migration[5.2]
  def change
  	change_column :work_orders, :instructions, :text
  end
end
