class AddContactNametoSitesAndClient < ActiveRecord::Migration[5.2]
  def change

  	add_column :branches, :contact_name, :string, after: :email
  	add_column :clients, :contact_name, :string, after: :email
  end
end
