class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.references :user
      t.string :name
      t.boolean :status

      t.timestamps
    end
  end
end
