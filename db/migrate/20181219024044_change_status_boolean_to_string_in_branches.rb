class ChangeStatusBooleanToStringInBranches < ActiveRecord::Migration[5.2]
  def change
    change_table :branches do |t|
      t.remove :status
    end  
      add_column :branches, :status, :string
  end
end
