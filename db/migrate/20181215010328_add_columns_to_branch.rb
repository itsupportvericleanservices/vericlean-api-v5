class AddColumnsToBranch < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :frequency, :string, after: 'client_id'
    add_column :branches, :weekly_frequency, :string, after: 'frequency'
  end
end
