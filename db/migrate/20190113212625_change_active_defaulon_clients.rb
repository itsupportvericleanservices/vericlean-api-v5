class ChangeActiveDefaulonClients < ActiveRecord::Migration[5.2]
  def change
    change_column :clients, :active, :integer, limit: 1, default: 1, after: :status
  end
end
