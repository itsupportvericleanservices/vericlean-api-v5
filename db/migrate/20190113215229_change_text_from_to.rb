class ChangeTextFromTo < ActiveRecord::Migration[5.2]
  def change
    change_column :text_messages, :from, :string, after: :sent_to
  end
end
