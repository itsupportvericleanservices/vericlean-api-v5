class AddTaskNameToWorkOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :work_orders, :task_name, :string
  end
end
