class RemoveReferenceFrecuencyToBranch < ActiveRecord::Migration[5.2]
  def change
    change_table :branches do |t|
      t.remove :frequency_id
    end  
  end
end
