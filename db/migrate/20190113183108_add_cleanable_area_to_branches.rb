class AddCleanableAreaToBranches < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :cleanable_area, :integer, after: :site_code
    change_column :branches, :created_at, :datetime, after: :muted
    change_column :branches, :updated_at, :datetime, after: :created_at
  end
end
