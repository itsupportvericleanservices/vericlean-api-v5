class DeleteRouteToBranches < ActiveRecord::Migration[5.2]
  def change
    change_table :branches do |t|
      t.remove :route_id
    end  
  end
end
