class AddTextMessagesTwilioBody < ActiveRecord::Migration[5.2]
  def change
    add_column :text_messages, :twilio_body, :text, after: 'body'
  end
end
