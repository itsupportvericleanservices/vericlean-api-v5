class CreateClients < ActiveRecord::Migration[5.2]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :address
      t.string :address2
      t.string :state
      t.string :city
      t.string :zipcode
      t.string :phone
      t.string :email
      t.boolean :status

      t.timestamps
    end
  end
end
