class AddFrecuencyToBranch < ActiveRecord::Migration[5.2]
  def change
    add_reference :branches, :frequency, after: :client_id
  end
end
