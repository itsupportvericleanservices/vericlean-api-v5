class UpdateStatusBooleanToIntHistories < ActiveRecord::Migration[5.2]
  def change
    change_table :route_histories do |t|
      t.remove :status
    end  
    add_column :route_histories, :status, :int
  end
end
