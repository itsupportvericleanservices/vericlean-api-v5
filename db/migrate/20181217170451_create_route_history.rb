class CreateRouteHistory < ActiveRecord::Migration[5.2]
  def change
    create_table :route_histories do |t|
      t.references :route_schedule
      t.references :branch
      t.references :user
      t.datetime :started_at
      t.datetime :finished_at
      t.boolean :status

      t.timestamps
    end
  end
end
