class AddReferenceUserComment < ActiveRecord::Migration[5.2]
  def change
  	add_reference :comments, :user, after: :id
  end
end
