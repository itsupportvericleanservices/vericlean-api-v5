class ChangeTypeWorkOrders < ActiveRecord::Migration[5.2]
  def change
  	rename_column :work_orders, :type, :main_type
  end
end
