class AddFieldDateToRouteHistories < ActiveRecord::Migration[5.2]
  def change
  	 add_column :route_histories, :scheduled_date, :date, after: :area_manager_id
  end
end
