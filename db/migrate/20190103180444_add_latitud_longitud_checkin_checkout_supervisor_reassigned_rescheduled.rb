class AddLatitudLongitudCheckinCheckoutSupervisorReassignedRescheduled < ActiveRecord::Migration[5.2]
  def change
    add_column :route_histories, :latitude_checkin, :string
    add_column :route_histories, :longitude_checkin, :string
    add_column :route_histories, :latitude_checkout, :string
    add_column :route_histories, :longitude_checkout, :string
    add_column :route_histories, :supervisor, :boolean
    add_column :route_histories, :reassigned, :boolean
    add_column :route_histories, :rescheduled, :boolean
  end
end
