class AddRouteScheduleToBranches < ActiveRecord::Migration[5.2]
  def change
    add_reference :branches, :route_schedule, after: :frequency_id
  end
end
