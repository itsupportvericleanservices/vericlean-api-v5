class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
    	t.references :branche
    	t.string :request_contact
    	t.string :request_contact_email
    	t.string :request_contact_phone
    	t.datetime :event_date
    	t.datetime :due_date
    	t.string :short_description
    	t.string :description
    	t.integer :status

    	t.timestamps

    end
  end
end
