class AddUserAreaManagerSupervisorSecundaryToBranch < ActiveRecord::Migration[5.2]
  def change
    add_reference :branches, :cleaner
    add_reference :branches, :area_manager
    add_reference :branches, :supervisor
  end
end
