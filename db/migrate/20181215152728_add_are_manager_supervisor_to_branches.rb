class AddAreManagerSupervisorToBranches < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :area_manager, :string
    add_column :branches, :supervisor, :string
  end
end
