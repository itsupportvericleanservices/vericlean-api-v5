class CreateServicetypes < ActiveRecord::Migration[5.2]
  def change
    create_table :servicetypes do |t|
      t.string :name
      t.integer :min_time
      t.integer :max_time

      t.timestamps
    end
  end
end
