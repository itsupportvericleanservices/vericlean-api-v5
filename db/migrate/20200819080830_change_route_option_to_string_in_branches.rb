class ChangeRouteOptionToStringInBranches < ActiveRecord::Migration[5.2]
  def change
    change_column :branches, :route_option, :string
  end
end
