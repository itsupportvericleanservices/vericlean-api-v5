class AddReferenceToTicket < ActiveRecord::Migration[5.2]
  def change
  	add_reference :tickets, :user, after: :branch_id
  end
end
