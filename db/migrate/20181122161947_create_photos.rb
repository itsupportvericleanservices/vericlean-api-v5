class CreatePhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :photos do |t|
      t.references :user
      t.references :service

      t.timestamps
    end
  end
end
