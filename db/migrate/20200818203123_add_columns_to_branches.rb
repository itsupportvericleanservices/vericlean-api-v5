class AddColumnsToBranches < ActiveRecord::Migration[5.2]
  def change
    rename_column :branches, :region, :subregion
    add_column :branches, :region, :string, after: :phone
    add_column :branches, :time_per_cleaning, :decimal, after: :cleanable_area, precision: 20, scale: 2
    add_column :branches, :start_time, :timestamp, after: :time_per_cleaning
    add_column :branches, :end_time, :timestamp, after: :start_time
    add_column :branches, :route_option, :timestamp, after: :end_time
  end
end
