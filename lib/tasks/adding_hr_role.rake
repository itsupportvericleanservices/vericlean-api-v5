namespace(:adding_hr_role) do
  desc('Adding Human Resources Role')
  task(run: :environment) do
    desc('Adding role')
    Role.find_or_create_by({name: 'HR'})
  end
end