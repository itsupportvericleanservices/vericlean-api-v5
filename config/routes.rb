Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  resources :users
  resources :roles

  get "clients/active" => "clients#active"
  get "clients/inactive" => "clients#inactive"
  resources :clients

  resources :frequencies
  resources :service_types
  resources :services
  resources :comments
  resources :photos
  resources :cleaners
  resources :area_managers
  resources :supervisors
  resources :active_services

  post "tickets/addcomment" => "tickets#add_comment"
  resources :tickets


  post "work_orders/email" => "work_orders#email"
  resources :work_orders
  resources :notifications

  post "route_histories/reassigned" => "route_histories#reassigned"
  post "route_histories/rescheduled" => "route_histories#rescheduled"
  resources :route_histories

  get "branches/states" => "branches#states"
  get "branches/cities" => "branches#cities"
  get "branches/active" => "branches#active"
  get "export_active_sites" => "branches#export_active_sites"
  resources :branches

  resources :building_types

  # Geofence
  get 'geofence', to: 'branches#geofence'
  get 'mygeofence', to: 'branches#mygeofence'
  get 'sitesgeofence', to: 'branches#sitesgeofence'
  get 'geofence_supervisor', to: 'branches#geofenceSupervisor'


  get "notifications/delayed/:id" => "notifications#delayed"
  get "branches/:id/notification_after" => "branches#notification_after"
  get "branches/:id/notification_after_duration" => "branches#notification_after_duration"
  get 'users_today' => 'users#user_for_today'
  get 'getsupervisor_all', to: 'branches#getsupervisorall'
  get 'getnewbranches', to:'branches#getnewbranches'
  get 'users/:id/token_notification', to:'users#token_notification'

  get "route_histories/:id/:filter_by/getbybranch" => "route_histories#getbybranch"
  get "branches/:id/getbycleaner" => "branches#getbycleaner"
  get "branches/:id/:lat/:lng/getbycleanerinclude" => "branches#getbycleanerinclude"
  get "branches/:id/:lat/:lng/getbysupervisorinclude" => "branches#getbysupervisorinclude"
  get "branches/:id/getbysupervisor" => "branches#getbysupervisor"
  get "branches/paginate/:page" => "branches#paginate"
  get "branches/inactive/paginate/:page" => "branches#inactive"
  get "users/:f/:date/getbyfrecuency" => "users#getbyfrecuency"

  get "branches/:id/pendding" => "branches#pendding"

  get "clients/sites/:id" => "clients#sites"

  get "work_orders/paginate/:page" => "work_orders#paginate"
  get "work_orders/:id/getfiles" => "work_orders#getfiles"

  get "clients/filter/:status" => "clients#status"
  get "clients/customers/filter/:status" => "clients#customer_status"
  get "clients/client_or_customer/:id" => "clients#client_or_customer_filter"
  get "clients/customers/filter/:client_id/:active" => "clients#customer_for_client"
  get "clients/all/:indirect" => "clients#all_client_or_customer"
  get "branches/:site_code/getbysitecode" => "branches#getbysitecode"
  post "branches/:id/request_update_location" => "branches#requestUpdateLocations"
  get "showRequestList" => "branches#showRequestList"
  get "getWOForUser/:usr" => "work_orders#wo_for_user"


  get "direct_client" => "branches#direct"
  get "indirect_client" => "branches#indirect"

  get "no_chekin/:by/:id/:start/:finish" => "route_histories#no_chekin"
  get "by_supervisor/:id/:start/:finish" => "route_histories#by_supervisor"
  get "by_cleaner/:id/:branch/:by/:start/:finish" => "route_histories#by_cleaner"
  get "by_all_check/:by/:id/:start/:finish" => "route_histories#by_all_check"

  get "route_histories/:print/:from_date/:to_date/paginate/:page" => "route_histories#paginate"

end
