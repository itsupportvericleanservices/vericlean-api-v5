require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)



module VericleanApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    
    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    config.time_zone = 'Central Time (US & Canada)'
    config.active_record.default_timezone = :local
    # config.active_job.queue_adapter = :sidekiq
    config.active_job.queue_adapter = Rails.env.production? ? :sidekiq : :async
    config.filter_parameters << lambda do |k, v|
      if k == 'file' && v && v.class == String && v.length > 1024
        v.replace('[BASE 64 FILTERED]')
      end
    end


    # EMAIL 
  config.action_mailer.delivery_method = :sendgrid_actionmailer
  config.action_mailer.sendgrid_actionmailer_settings = {
    api_key: "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",
    raise_delivery_errors: true
  }
  end
end
