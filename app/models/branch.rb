class Branch < ApplicationRecord
  default_scope { where(deleted: 0) }
  scope :active, -> {where(active: true)}
  belongs_to :client
  has_and_belongs_to_many :service_types,join_table:"branches_service_types"
  has_and_belongs_to_many :extra_cleaners, join_table:"branches_extra_cleaners", association_foreign_key: 'user_id'
  has_and_belongs_to_many :extra_area_managers, join_table:"branches_extra_area_managers", association_foreign_key: 'user_id'
  has_and_belongs_to_many :extra_supervisors, join_table:"branches_extra_supervisors", association_foreign_key: 'user_id'

  has_many :route_history
  belongs_to :cleaner, optional: true
  belongs_to :area_manager, optional: true
  belongs_to :supervisor, optional: true
  belongs_to :building_type, optional: true
  has_many :comments, as: :commentable
end
