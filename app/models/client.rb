class Client < ApplicationRecord
  has_many :users
  has_many :branches
  has_many :indirect_clients
end
