class Supervisor < User
  default_scope { where(role_id: 5) }

  has_many :branches
end