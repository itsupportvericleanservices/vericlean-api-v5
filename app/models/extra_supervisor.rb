class ExtraSupervisor < User
  default_scope { where(role_id: 5) }
end