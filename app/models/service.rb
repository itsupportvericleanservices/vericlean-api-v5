class Service < ApplicationRecord
  belongs_to :branch
  has_many :comments
  has_many :photos
  has_and_belongs_to_many :service_types,join_table:"service_types_services"
end
