class Cleaner < User
  default_scope { where(role_id: 6) }

  has_many :branches
end