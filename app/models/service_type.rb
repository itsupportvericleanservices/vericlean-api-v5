class ServiceType < ApplicationRecord
  has_and_belongs_to_many :branches,join_table:"branches_service_types"
  has_and_belongs_to_many :services,join_table:"service_types_services"
end
