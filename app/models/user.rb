class User < ApplicationRecord
  # Include default devise modules.
  # :confirmable, :omniauthable
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable
  include DeviseTokenAuth::Concerns::User

  belongs_to :role
  belongs_to :client, optional: true
 
  has_many :comments
  has_many :photos
  has_many :photos
  has_many :route_history
  has_many :notifications

  def is_superadmin?
    self.role_id == 1
  end

  def is_admin?
    self.role_id == 2
  end

  private
    def set_default_role
      self.role ||= Role.find_by_name('user')
    end
end
