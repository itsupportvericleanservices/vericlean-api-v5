class CheckToday < RouteHistory
  default_scope { where("started_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}' AND status <> 0 AND status is not NULL ") }

  belongs_to :user
  belongs_to :branch
end