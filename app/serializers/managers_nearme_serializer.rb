class ManagersNearmeSerializer < ActiveModel::Serializer
  attributes :id, :name, :site_code, :address, :address2, :state, :city, :zipcode, :max_time, :duration_min, :duration_max, :route_history_id, :service_status, :service_started, :service_finished, :working_seconds, :nearme
  belongs_to :client, serializer: ClientInfoSerializer

  def nearme # Fixed always true
    true
  end

  def service_status
    service_status = "pending"
    route_histories = @instance_options[:route_histories]
    route_histories.each do |rh|
      if rh.branch_id == (object.id)
        service_status = "in_progress"
        if rh.started_at.nil?
          service_status = "pending"
        end

        if !rh.finished_at.nil?
          service_status = "complete"
        end
      end
    end
    service_status
  end

  def service_started
    service_started = ""
    route_histories = @instance_options[:route_histories]
    route_histories.each do |rh|
      if rh.branch_id == (object.id)
        if !rh.started_at.nil?
          service_started = rh.started_at
        end
      end
    end
    service_started
  end

  def service_finished
    service_finished = ""
    route_histories = @instance_options[:route_histories]
    route_histories.each do |rh|
      if rh.branch_id == (object.id)
        if !rh.finished_at.nil?
          service_finished = rh.finished_at
        end
      end
    end
    service_finished
  end

  def working_seconds
    working_seconds = 0
    route_histories = @instance_options[:route_histories]
    route_histories.each do |rh|
      if rh.branch_id == (object.id)
        if !rh.started_at.nil?
          working_seconds = (DateTime.now.in_time_zone - rh.started_at).to_i
        end
      end
    end
    working_seconds
  end

  def route_history_id
    route_history_id = ""
    route_histories = @instance_options[:route_histories]
    route_histories.each do |rh|
      if rh.branch_id == (object.id)
        if !rh.started_at.nil? && rh.finished_at.nil?
          route_history_id = rh.id
        end
      end
    end
    route_history_id
  end

end
