class WorkOrdersNearmeSerializer < ActiveModel::Serializer
  attributes :id, :status, :nearme, :task_name, :task_description, :site_contact, :site_phone, :main_type, :priority, :comments
  belongs_to :branch, serializer: BranchInfoSerializer

  def nearme
    nearme_ids = @instance_options[:nearme_ids]
    nearme_ids.include?(object.branch_id)
  end

end
