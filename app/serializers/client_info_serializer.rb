class ClientInfoSerializer < ActiveModel::Serializer
  attributes :id, :name
end
