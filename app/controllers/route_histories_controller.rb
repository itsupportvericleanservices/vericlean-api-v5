class RouteHistoriesController < ApplicationController
  before_action :authenticate_user!, except: [:by_supervisor,:no_chekin,:no_chekin,:by_all_check,:by_cleaner]
  before_action :set_route_history, only: [:show, :update, :destroy]

  def index
    @route_history = RouteHistory.all

    render json: @route_history, include: [:user]
  end

  def show
    render json: @route_history, include: [:user]
  end

  def create
    @branch = Branch.find(route_history_params['branch_id']);
    if route_history_params["assigned_id"].nil?
      assigned_id = route_history_params["user_id"]
    else
      assigned_id = route_history_params["assigned_id"]
    end

    if route_history_params["delayed"].nil?
      delayed_flag = 0
    else
      delayed_flag = route_history_params["delayed"]
    end

    route_history_params_aux = {
      user_id: route_history_params["user_id"],
      assigned_id: assigned_id,
      cleaner_id: @branch.cleaner_id,
      supervisor_id: @branch.supervisor_id,
      area_manager_id: @branch.area_manager_id,
      branch_id: route_history_params["branch_id"],
      started_at: DateTime.now.in_time_zone,
      status: route_history_params["status"],
      supervisor: route_history_params["supervisor"],
      latitude_checkin: route_history_params["latitude_checkin"],
      longitude_checkin: route_history_params["longitude_checkin"],
      delayed: delayed_flag,
    }

    @route_history = RouteHistory.new(route_history_params_aux)

    if @route_history.save
      render json: @route_history, status: :created
    else
      render json: @route_history.errors, status: :unprocessable_entity
    end
  end

  def update
    route_history_params_aux = route_history_params

    if !params['started_at'].nil?
      route_history_params_aux['started_at'] = DateTime.now.in_time_zone
     end

    if !params['finished_at'].nil?
      route_history_params_aux['finished_at'] = DateTime.now.in_time_zone
     end




    if @route_history.update(route_history_params_aux)

      render json: @route_history
    else
      render json: @route_history.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @route_history.destroy
      render json: true
    else
      render json: false
    end
  end

  def getbybranch
    @id = params['id'];

    histories=[]

    if params[:filter_by] == 'today'
      histories = RouteHistory.all.where(branch_id: @id).where("created_at BETWEEN '#{DateTime.now}' AND '#{DateTime.now}'").order(created_at: :ASC)
    elsif params[:filter_by] == 'week'
      histories = RouteHistory.all.where(branch_id: @id).where("created_at BETWEEN '#{DateTime.now.beginning_of_week}' AND '#{DateTime.now.end_of_week}'").order(created_at: :ASC)
    elsif params[:filter_by] == 'month'
      histories = RouteHistory.all.where(branch_id: @id).where("created_at BETWEEN '#{DateTime.now.beginning_of_month}' AND '#{DateTime.now.end_of_month}'").order(created_at: :ASC)
    elsif params[:filter_by] == 'year'
      histories = RouteHistory.all.where(branch_id: @id).where("created_at BETWEEN '#{DateTime.now.beginning_of_year}' AND '#{DateTime.now.end_of_year}'").order(created_at: :ASC)
    elsif params[:filter_by] == 'range'
      range_start = params[:range_start]
      range_end = params[:range_end]
      histories = RouteHistory.all.where(branch_id: @id).where("created_at BETWEEN '#{range_start}' AND '#{range_end}'").order(created_at: :ASC)
    end

    @response={
      "histories": histories
    }

    render json:@response
  end

  def reassigned
    @branch_id = params['branch_id'];
    @user_id = params['user_id'];
    scheduled_date = params['scheduled_date'];
    assigned_id = params['assigned_id']

    route_history = RouteHistory.all.where("branch_id = #{@branch_id} AND status = 0 AND reassigned = 1 ").length

    if route_history > 0

      reassigned = RouteHistory.all.where("branch_id = #{@branch_id} AND status = 0 AND reassigned = 1 ").update_all(:user_id => @user_id, :branch_id => @branch_id, :assigned_id => assigned_id, :scheduled_date => scheduled_date)

      render json: reassigned

    else

      reassigned = RouteHistory.new(:user_id => @user_id, :branch_id => @branch_id, :assigned_id => assigned_id, :scheduled_date => scheduled_date, :status => 0, :reassigned => 1)

      if reassigned.save
        render json: reassigned
      end

    end

  end

  def rescheduled
    @branch_id = params['branch_id'];
    @user_id = params['user_id'];
    scheduled_date = params['scheduled_date'];
    assigned_id = params['assigned_id']

    @rescheduled = RouteHistory.new(:user_id => @user_id, :branch_id => @branch_id, :assigned_id => assigned_id, :scheduled_date => scheduled_date, :status => 0, :rescheduled => 1)
    if @rescheduled.save
      render json: @rescheduled
    end

  end

  def paginate
    user = @current_user
    role = Role.find(user.role_id)

    # from = params['from_date'].in_time_zone
    # to = params['to_date'].in_time_zone
    from = DateTime.parse(params['from_date']).beginning_of_day  + (6.0/24)
    to = DateTime.parse(params['to_date']).end_of_day  + (6.0/24)
    if params['print'] == '1'
      # binding.pry
      if params['filter'].nil?
        @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}'").order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)
      else
        # if params['filter_by'] == 'client'
        #   # @branches = Branch.all.where(client_id: params['client_id']).where(active: 1)
        #   @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.client_id = ?",params['client_id']).order(started_at: :ASC)

        # end
        if params['filter_by'] == 'customer' || params['filter_by'] == 'customer_for_client' || params['filter_by'] == 'client'
          # @branches = Branch.all.where(client_id: params['customer_id']).where(active: 1)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.client_id = ?",params['filter_data']).order(started_at: :ASC)

        end

        if params['filter_by'] == 'cleaner'
          # @branches = Branch.all.where(cleaner_id: params['cleaner_id']).where(active: 1)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND route_histories.user_id = ?",params['cleaner_id']).order(started_at: :ASC)

        end

        if params['filter_by'] == 'state'
          # @branches = Branch.all.where(state: params['state']).where(active: 1)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.state = ?",params['state']).order(started_at: :ASC)

        end

        if params['filter_by'] == 'city'

          # @branches = Branch.all.where(city: params['city']).where(active: 1)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.city = ?",params['city']).order(started_at: :ASC)
        end
        if params['filter_by'] == 'supervisor_sites'

          # @branches = Branch.all.where(city: params['city']).where(active: 1)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.supervisor_id = ?",params['supervisor_id']).order(started_at: :ASC)
        end
        if params['filter_by'] == 'supervisor'

          # @branches = Branch.all.where(city: params['city']).where(active: 1)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND route_histories.user_id = ?",params['supervisor_id']).order(started_at: :ASC)
        end
        if params['filter_by'] == 'checking_by'

          case params['checking_by']
          when 'cleaner'
            @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}' AND supervisor is NULL AND reassigned is NULL").order(started_at: :ASC)

          when 'supervisor'
            @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}' AND supervisor = 1").order(started_at: :ASC)

          else
            @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}' AND reassigned = 1").order(started_at: :ASC)

          end

        end
      end
    else

      if params['filter'].nil?
        @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}'").order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)
      else
        # if params['filter_by'] == 'client'
        #   # @branches = Branch.all.where(client_id: params['client_id']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
        #   @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.client_id = ?",params['client_id']).order(started_at: :ASC).paginate(:page => params[:page], :per_page => 50)

        # end
        if params['filter_by'] == 'customer' || params['filter_by'] == 'customer_for_client' || params['filter_by'] == 'client'
          # @branches = Branch.all.where(client_id: params['customer_id']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.client_id = ?",params['filter_data']).order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)

        end

        if params['filter_by'] == 'cleaner'
          # @branches = Branch.all.where(cleaner_id: params['cleaner_id']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND route_histories.user_id = ?",params['cleaner_id']).order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)

        end

        if params['filter_by'] == 'state'
          # @branches = Branch.all.where(state: params['state']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.state = ?",params['state']).order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)

        end

        if params['filter_by'] == 'city'

          # @branches = Branch.all.where(city: params['city']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.city = ?",params['city']).order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)
        end
        if params['filter_by'] == 'supervisor_sites'

          # @branches = Branch.all.where(city: params['city']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND branches.supervisor_id = ?",params['supervisor_id']).order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)
        end
        if params['filter_by'] == 'supervisor'

          # @branches = Branch.all.where(city: params['city']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
          @checks = RouteHistory.joins('LEFT JOIN branches ON branches.id = route_histories.branch_id').where("route_histories.started_at BETWEEN '#{from}' AND '#{to}' AND route_histories.user_id = ?",params['supervisor_id']).order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)
        end
        if params['filter_by'] == 'checking_by'

          case params['checking_by']
          when 'cleaner'
            @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}' AND supervisor is NULL AND reassigned is NULL").order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)

          when 'supervisor'
            @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}' AND supervisor = 1").order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)

          else
            @checks = RouteHistory.where("started_at BETWEEN '#{from}' AND '#{to}' AND reassigned = 1").order(started_at: :ASC).paginate(:page => params[:page], :per_page => 300)

          end

        end
      end
    end

    @checks_resp = []
    @checks.each do |check|

      br = Branch.find_by_id(check.branch_id)
      if br   
        cl = Client.find(br.client_id)
      end
      u = User.find(check.user_id)

      if !check.supervisor_id.nil?
        supervisor = User.find(check.supervisor_id)
      elsif !br&.supervisor_id.nil?
        supervisor = User.find(br.supervisor_id)
      else
        supervisor = ''
      end

      if !check.area_manager_id.nil?
        manager = User.find(check.area_manager_id)
      elsif !br&.area_manager_id.nil?
        manager = User.find(br.area_manager_id)
      else
        manager = ''
      end

      if !check.cleaner_id.nil?
        cln = User.find(check.cleaner_id)
      elsif !br&.cleaner_id.nil?
        cln = User.find(br.cleaner_id)
      else
        cleaner = ''
      end
      

      @check_aux = {
        check: check,
        user: u,
        branch: br,
        client: cl,
        supervisor: supervisor,
        area_manager: manager,
        cleaner: cln,
        page:params[:page]

      }
      @checks_resp << @check_aux
    end
    # render json: [@checks, :page => params[:page]], include: [:branch, :user]
    render json: @checks_resp
  end

  def no_chekin
    start =  params['start'].to_time
    finish =  params['finish'].to_time
    by = params['by']
    id = params['id']
    whitout_checks = Hash.new
    
    while start <= finish  do
     
      fecha =start.strftime('%A')+"-"+start.strftime('%Y-%m-%d') 

      whitout_checks[fecha]= Array.new
      chekins_data = RouteHistory.all.where("started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}' AND finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}'").pluck(:branch_id)
      
      start_n = start.strftime('%u')

      if id.to_i == 0

        branch_query = 'SELECT clients.name as "client_name", clients.indirect as "client_indirect", 
        CONCAT(cleaners.first_name, " ", cleaners.last_name) as "cleaner_name", cleaners.phone as "cleaner_phone",
        CONCAT(supervisors.first_name, " ", supervisors.last_name) as "supervisor_name", supervisors.phone as "supervisor_phone",
        CONCAT(areamanagers.first_name, " ", areamanagers.last_name) as "areamanager_name", areamanagers.phone as "areamanager_phone",
        branches.* FROM branches 
        LEFT JOIN clients ON clients.id = branches.client_id
        LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
        LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
        LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id WHERE branches.frequency LIKE "%'+ start_n +'%"'
        branches = Branch.find_by_sql(branch_query)

      elsif by == 'client' &&  id.to_i > 0

        branch_query = 'SELECT clients.name as "client_name", clients.indirect as "client_indirect", 
        CONCAT(cleaners.first_name, " ", cleaners.last_name) as "cleaner_name", cleaners.phone as "cleaner_phone",
        CONCAT(supervisors.first_name, " ", supervisors.last_name) as "supervisor_name", supervisors.phone as "supervisor_phone",
        CONCAT(areamanagers.first_name, " ", areamanagers.last_name) as "areamanager_name", areamanagers.phone as "areamanager_phone",
        branches.* FROM branches 
        LEFT JOIN clients ON clients.id = branches.client_id
        LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
        LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
        LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id WHERE branches.frequency LIKE "%'+ start_n +'%" and branches.active =1 and branches.deleted = 0 and branches.client_id = "'+ id +'"'
        branches = Branch.find_by_sql(branch_query)

      elsif by == 'sites' && id.to_i > 0

        branch_query = 'SELECT clients.name as "client_name", clients.indirect as "client_indirect", 
        CONCAT(cleaners.first_name, " ", cleaners.last_name) as "cleaner_name", cleaners.phone as "cleaner_phone",
        CONCAT(supervisors.first_name, " ", supervisors.last_name) as "supervisor_name", supervisors.phone as "supervisor_phone",
        CONCAT(areamanagers.first_name, " ", areamanagers.last_name) as "areamanager_name", areamanagers.phone as "areamanager_phone",
        branches.* FROM branches 
        LEFT JOIN clients ON clients.id = branches.client_id
        LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
        LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
        LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id WHERE branches.frequency LIKE "%'+ start_n +'%" and branches.active =1 and branches.deleted = 0 and branches.id = "'+ id +'"'
        branches = Branch.find_by_sql(branch_query)
      
      elsif by == 'supervisor' && id.to_i > 0

        branch_query = 'SELECT clients.name as "client_name", clients.indirect as "client_indirect", 
        CONCAT(cleaners.first_name, " ", cleaners.last_name) as "cleaner_name", cleaners.phone as "cleaner_phone",
        CONCAT(supervisors.first_name, " ", supervisors.last_name) as "supervisor_name", supervisors.phone as "supervisor_phone",
        CONCAT(areamanagers.first_name, " ", areamanagers.last_name) as "areamanager_name", areamanagers.phone as "areamanager_phone",
        branches.* FROM branches 
        LEFT JOIN clients ON clients.id = branches.client_id
        LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
        LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
        LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id WHERE branches.frequency LIKE "%'+ start_n +'%" and branches.active =1 and branches.deleted = 0 and branches.supervisor_id = "'+ id +'"'
        branches = Branch.find_by_sql(branch_query)

      elsif by == 'cleaner' && id.to_i > 0

        branch_query = 'SELECT clients.name as "client_name", clients.indirect as "client_indirect", 
        CONCAT(cleaners.first_name, " ", cleaners.last_name) as "cleaner_name", cleaners.phone as "cleaner_phone",
        CONCAT(supervisors.first_name, " ", supervisors.last_name) as "supervisor_name", supervisors.phone as "supervisor_phone",
        CONCAT(areamanagers.first_name, " ", areamanagers.last_name) as "areamanager_name", areamanagers.phone as "areamanager_phone",
        branches.* FROM branches 
        LEFT JOIN clients ON clients.id = branches.client_id
        LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
        LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
        LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id WHERE branches.frequency LIKE "%'+ start_n +'%" and branches.active =1 and branches.deleted = 0 and branches.cleaner_id = "'+ id +'"'
        branches = Branch.find_by_sql(branch_query)  

      end
      
      if branches.length > 0 
        branches.each do |bc|
          if !chekins_data.include? bc.id         
            whitout_checks[fecha].push(bc) 
          end
        end
      end

      start = (start + 1.day)

    end
    
    sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name)
    clients = Client.all.where(active: 1).where(deleted: 0).pluck(:id,:name)
    cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
    supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)
    

    reponse={
      'sites': sites,
      'checkins': whitout_checks,
      'clients': clients,
      'cleaners': cleaners,
      'supervisors': supervisors
    }

    render json: reponse
  end

  def by_supervisor
    id = params['id']
    start =  params['start'].to_time
    finish =  params['finish'].to_time

    supervisores = Supervisor.all.where(role_id: 5).where(deleted: 0).pluck(:id,:first_name,:last_name)

    if id.to_i > 0
      puts "0"
      checkins = RouteHistory.find_by_sql("SELECT route_histories.id as route_histories_id,route_histories.started_at as route_histories_started, route_histories.finished_at as route_histories_finished,route_histories.status as route_histories_status,  clients.name as 'client_name', clients.indirect as 'client_indirect', 
      CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name', cleaners.phone as 'cleaner_phone',
      CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name', supervisors.phone as 'supervisor_phone',
      CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name', areamanagers.phone as 'areamanager_phone',
      CONCAT(assigned.first_name, ' ', assigned.last_name) as 'assigned_name',
      branches.* FROM branches 
      left join route_histories on route_histories.branch_id = branches.id
      LEFT JOIN clients ON clients.id = branches.client_id
      LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
      LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
      LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id 
      LEFT JOIN users as assigned ON assigned.id = route_histories.assigned_id WHERE route_histories.assigned_id = '#{id}' AND route_histories.supervisor = 1 
      AND route_histories.status > 0 and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'")

      all = checkins.length
      completes = []
      incompletes=[]

      checkins.each do |ch|
        if ch.route_histories_finished == nil
          incompletes << ch
        elsif ch.route_histories_finished != nil
          completes << ch
        end
      end

    elsif id == '-1'
      puts "-1"
      checkins = RouteHistory.find_by_sql("SELECT route_histories.id as route_histories_id,route_histories.started_at as route_histories_started, route_histories.finished_at as route_histories_finished,route_histories.status as route_histories_status,  clients.name as 'client_name', clients.indirect as 'client_indirect', 
        CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name', cleaners.phone as 'cleaner_phone',
        CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name', supervisors.phone as 'supervisor_phone',
        CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name', areamanagers.phone as 'areamanager_phone',
        CONCAT(assigned.first_name, ' ', assigned.last_name) as 'assigned_name',
        branches.* FROM branches 
        left join route_histories on route_histories.branch_id = branches.id
        LEFT JOIN clients ON clients.id = branches.client_id
        LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
        LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
        LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id 
        LEFT JOIN users as assigned ON assigned.id = route_histories.assigned_id WHERE route_histories.supervisor = 1 
        AND route_histories.status > 0 and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'")
  
        all = checkins.length
        completes = []
        incompletes=[]
  
        checkins.each do |ch|
          if ch.route_histories_finished == nil
            incompletes << ch
          elsif ch.route_histories_finished != nil
            completes << ch
          end
        end
    else
      puts "na"
      checkins = nil
      all = 0
      completes = []
      incompletes=[]
    end

      reponse={
        'checkins': checkins,
        'supervisors': supervisores,
        'all': all,
        'completes': completes.length,
        'incompletes': incompletes.length 
        
      }
  
      render json: reponse
  end

  def by_cleaner
    id = params['id']
    branch = params['branch']
    start =  params['start'].to_time
    finish =  params['finish'].to_time
    by = params['by']

    if by == 'cleaner'
      
    if id.to_i > 0
      if branch.to_i > 0
        puts 'by'
        checkins = RouteHistory.find_by_sql("SELECT route_histories.id as 'route_histories_id',route_histories.started_at as 'route_histories_started', 
          route_histories.finished_at as 'route_histories_finished',route_histories.status as 'route_histories_status',
          route_histories.branch_id as 'route_histories_branch',  clients.name as 'client_name', clients.indirect as 'client_indirect', 
            CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name',
            CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name',
            CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name',
            branches.* FROM branches 
          left join route_histories on route_histories.branch_id = branches.id
            LEFT JOIN clients ON clients.id = branches.client_id
            LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
            LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
            LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id where route_histories.assigned_id = '#{id}' and route_histories.branch_id = '#{branch}'
        and route_histories.status = 2 and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'  and route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'
        and route_histories.supervisor IS NULL")
    
        cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
        sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
        supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)

      elsif branch == '-1'
        puts 'all'
        checkins = RouteHistory.find_by_sql("SELECT route_histories.id as 'route_histories_id',route_histories.started_at as 'route_histories_started', 
          route_histories.finished_at as 'route_histories_finished',route_histories.status as 'route_histories_status',
          route_histories.branch_id as 'route_histories_branch',  clients.name as 'client_name', clients.indirect as 'client_indirect', 
            CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name',
            CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name',
            CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name',
            branches.* FROM branches 
          left join route_histories on route_histories.branch_id = branches.id
            LEFT JOIN clients ON clients.id = branches.client_id
            LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
            LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
            LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id where route_histories.assigned_id = '#{id}'
            and route_histories.status = 2  and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'  and route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}' and route_histories.supervisor IS NULL")
        #and route_histories.started_at BETWEEN '2019-01-01' AND '2019-04-09'  and route_histories.finished_at BETWEEN '2019-01-01' AND '2019-04-09'
    
        cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
        sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
        supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)
      

      else
        puts 'nothing'
        checkins = []
        cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
        sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
        supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)
      
      end
    else
      puts 'all_clean'
      checkins = RouteHistory.find_by_sql("SELECT route_histories.id as 'route_histories_id',route_histories.started_at as 'route_histories_started', 
        route_histories.finished_at as 'route_histories_finished',route_histories.status as 'route_histories_status',
        route_histories.branch_id as 'route_histories_branch',  clients.name as 'client_name', clients.indirect as 'client_indirect', 
          CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name',
          CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name',
          CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name',
          branches.* FROM branches 
        left join route_histories on route_histories.branch_id = branches.id
          LEFT JOIN clients ON clients.id = branches.client_id
          LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
          LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
          LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id where 
       route_histories.status = 2 and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'  and route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'
      and route_histories.supervisor IS NULL")
  
      cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
      sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
      supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)
    end
    
      

    elsif by == 'supervisor'
      if id.to_i > 0
        if branch.to_i > 0
          puts 'by'
          checkins = RouteHistory.find_by_sql("SELECT route_histories.id as 'route_histories_id',route_histories.started_at as 'route_histories_started', 
            route_histories.finished_at as 'route_histories_finished',route_histories.status as 'route_histories_status',
            route_histories.branch_id as 'route_histories_branch',  clients.name as 'client_name', clients.indirect as 'client_indirect', 
              CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name',
              CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name',
              CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name',
              branches.* FROM branches 
            left join route_histories on route_histories.branch_id = branches.id
              LEFT JOIN clients ON clients.id = branches.client_id
              LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
              LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
              LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id where route_histories.assigned_id = '#{id}' and route_histories.branch_id = '#{branch}'
          and route_histories.status = 2 and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'  and route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}' and route_histories.supervisor = 1")
          #and route_histories.started_at BETWEEN '2019-01-01' AND '2019-04-09'  and route_histories.finished_at BETWEEN '2019-01-01' AND '2019-04-09'
      
          cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
          sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
          supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)

        elsif branch == '-1'
          puts 'all'
          checkins = RouteHistory.find_by_sql("SELECT route_histories.id as 'route_histories_id',route_histories.started_at as 'route_histories_started', 
            route_histories.finished_at as 'route_histories_finished',route_histories.status as 'route_histories_status',
            route_histories.branch_id as 'route_histories_branch',  clients.name as 'client_name', clients.indirect as 'client_indirect', 
              CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name',
              CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name',
              CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name',
              branches.* FROM branches 
            left join route_histories on route_histories.branch_id = branches.id
              LEFT JOIN clients ON clients.id = branches.client_id
              LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
              LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
              LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id where route_histories.assigned_id = '#{id}'
              and route_histories.status = 2 and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'  and route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}' and route_histories.supervisor = 1")
              
          #and route_histories.started_at BETWEEN '2019-01-01' AND '2019-04-09'  and route_histories.finished_at BETWEEN '2019-01-01' AND '2019-04-09'
      
          cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
          sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
          supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)
        

        else
          puts 'nothing'
          checkins = []
          cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
          sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
          supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)
        
        end
      else
        puts 'all_sup'
        checkins = RouteHistory.find_by_sql("SELECT route_histories.id as 'route_histories_id',route_histories.started_at as 'route_histories_started', 
          route_histories.finished_at as 'route_histories_finished',route_histories.status as 'route_histories_status',
          route_histories.branch_id as 'route_histories_branch',  clients.name as 'client_name', clients.indirect as 'client_indirect', 
            CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name',
            CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name',
            CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name',
            branches.* FROM branches 
          left join route_histories on route_histories.branch_id = branches.id
            LEFT JOIN clients ON clients.id = branches.client_id
            LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
            LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
            LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id where 
        route_histories.status = 2 and route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'  and route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{finish.end_of_day+(360*60)}'
        and route_histories.supervisor = 1")
    
        cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
        sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name,:cleaner_id,:supervisor_id)
        supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)
      end
    end

    reponse={
      'checkins': checkins,
      'cleaners': cleaners,
      'supervisors': supervisors,
      'all': sites,
      # 'completes': completes.length,
      # 'incompletes': incompletes.length 
      
    }
    render json: reponse
  end

  def by_all_check

    by = params['by']
    id = params['id']
    date_start =  params['start'].to_time
    date_end =  params['finish'].to_time
    branches_all_with_checks = Hash.new
    branches_all_without_checks = Hash.new
    branches_all_with_checks_count = 0
    branches_all_without_checks_count = 0

    if id.to_i == 0
      where= "branches.frequency LIKE '%#{date_start.strftime('%u')}%' AND branches.active =1"
      
    elsif by == 'client' &&  id.to_i > 0

      where = " branches.frequency LIKE '%#{date_start.strftime('%u')}%' AND branches.active =1 and branches.deleted = 0 and branches.client_id = '#{id}'"
    
    # binding.pry
      
    elsif by == 'sites' && id.to_i > 0

      where = "branches.frequency LIKE '%#{date_start.strftime('%u')}%' AND branches.active =1 and branches.deleted = 0 and branches.id = '#{id}'"

    elsif by == 'supervisor' && id.to_i > 0

      where = "branches.frequency LIKE '%#{date_start.strftime('%u')}%' AND branches.active =1 and branches.deleted = 0 and branches.supervisor_id = '#{id}'"

    elsif by == 'cleaner' && id.to_i > 0

      where = "branches.frequency LIKE '%#{date_start.strftime('%u')}%' AND branches.active =1 and branches.deleted = 0 and branches.cleaner_id = '#{id}'"
      
    end

    while date_start <= date_end  do   
      current_date = date_start.strftime('%A')+"-"+date_start.strftime('%Y-%m-%d') 

      # Get the branches
      today_branches = Branch.all.where(where)
      .joins("LEFT JOIN clients as client ON branches.client_id = client.id")
      .joins("LEFT JOIN users as cl ON branches.cleaner_id = cl.id")
      .joins("LEFT JOIN users as sup ON branches.supervisor_id = sup.id")
      .joins("LEFT JOIN users as am ON branches.area_manager_id = am.id")
      .select("branches.*, client.name as client_name, client.indirect as client_indirect, cl.id as cl_id, CONCAT(cl.first_name,  ' ', cl.last_name) as cl_name, sup.id as sup_id, CONCAT(sup.first_name,  ' ', sup.last_name) as sup_name, am.id as am_id, CONCAT(am.first_name,  ' ', am.last_name) as am_name")
      
      # Get the Checkins (Route Histories)
      today_route_histories = RouteHistory.all.where("started_at BETWEEN '#{date_start.beginning_of_day+(360*60)}' AND '#{date_start.end_of_day+(360*60)}' AND finished_at IS NOT NULL").where("route_histories.supervisor IS NULL")
      .joins("LEFT JOIN users as asg ON route_histories.assigned_id = asg.id")
      .select("route_histories.*, asg.id as asg_id, CONCAT(asg.first_name,  ' ', asg.last_name) as asg_name")
      .order(started_at: :ASC)

      branches_all_without_checks[current_date] = Array.new 
      branches_all_with_checks[current_date] = Array.new 
      today_branches.each do |branch|
        # Check if it has checkins
        rh_found = false
        today_route_histories.each do |rh|  
          if rh.branch_id == branch.id 
            rh_found = true
            branch_aux = Hash.new
          
            branch.attributes.each do |key, val|
              branch_aux[key] = val
            end
            branch_aux['checkins'] = (rh)
            branches_all_with_checks[current_date].push(branch_aux)
            branches_all_with_checks_count+=1
          end
        end

        # If no checkins
        if rh_found == false 
          branches_all_without_checks[current_date].push(branch)
          branches_all_without_checks_count+=1
        end
      end      
      date_start = (date_start + 1.day)
    end

    sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name)
    clients = Client.all.where(active: 1).where(deleted: 0).pluck(:id,:name)
    cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
    supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)

    reponse = {
      'sites': sites,
      'clients': clients,
      'cleaners': cleaners,
      'supervisors': supervisors,
      'without_checkins': branches_all_without_checks,
      'with_checkins': branches_all_with_checks,
      'without_checkins_count': branches_all_without_checks_count,
      'with_checkins_count': branches_all_with_checks_count,
      'total_checkins': branches_all_without_checks_count + branches_all_with_checks_count
    }

    render json: reponse
    ##
    ##
    ##
    ##
    ##
    # while start <= finish  do
     
    #   fecha =start.strftime('%A')+"-"+start.strftime('%Y-%m-%d') 

    #   without_checks[fecha]= Array.new
    #   with_checks[fecha]= Array.new

    #   chekins_data = RouteHistory.all.where("started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}' AND finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}'").pluck(:branch_id)
      
    #   start_n = start.strftime('%u')

    #   inicio = start.beginning_of_week.strftime('%Y-%m-%d')
    #   fin = start.end_of_week.strftime('%Y-%m-%d')
    #   #route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}' AND route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}'  AND
    #   if id.to_i == 0
    #     where= "WHERE branches.frequency LIKE '%#{start_n}%'"
        
    #   elsif by == 'client' &&  id.to_i > 0

    #     where = " WHERE branches.frequency LIKE '%#{start_n}%' AND branches.active =1 and branches.deleted = 0 and branches.client_id = '#{id}'"
      
    #   # binding.pry
        
    #   elsif by == 'sites'

    #     where = "WHERE branches.frequency LIKE '%#{start_n}%' AND branches.active =1 and branches.deleted = 0 and branches.id = '#{id}'"

    #   elsif by == 'supervisor' && id.to_i > 0

    #     where = "WHERE branches.frequency LIKE '%#{start_n}%' AND branches.active =1 and branches.deleted = 0 and branches.supervisor_id = '#{id}'"

    #   elsif by == 'cleaner' && id.to_i > 0

    #     where = "WHERE branches.frequency LIKE '%#{start_n}%' AND branches.active =1 and branches.deleted = 0 and branches.cleaner_id = '#{id}'"
        
    #   end


    #   branches = Branch.find_by_sql("SELECT clients.name as 'client_name', clients.indirect as 'client_indirect', 'client_indirect', route_histories.started_at as 'r_h_started',route_histories.finished_at as 'r_h_finished', route_histories.branch_id as 'r_h_branch_id',
    #     CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name', cleaners.phone as 'cleaner_phone',
    #     CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name', supervisors.phone as 'supervisor_phone',
    #     CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name', areamanagers.phone as 'areamanager_phone',
    #     CONCAT(assigned.first_name, ' ', assigned.last_name) as 'assigned_name',
    #     branches.* FROM branches 
    #     inner join route_histories on route_histories.branch_id = branches.id  
    #     LEFT JOIN clients ON clients.id = branches.client_id
    #     LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
    #     LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
    #     LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id
		#   left join users as assigned on assigned.id = route_histories.assigned_id #{where} ")
      
    #     ids_b = []

    #     if branches.length > 0 
    #       branches.each do |bc|
    #         if chekins_data.include? bc.id
    #           if !ids_b.include? bc.id
    #             ids_b << bc.id
    #           end
    #         end
    #       end
    #     end
        
    #     if branches.length > 0 
    #       branches.each do |bc|
    #         if !chekins_data.include? bc.id         
    #           without_checks[fecha].push(bc)
    #           branches_without << bc 
              
            
    #         end
    #       end
    #     end

    #     if ids_b.length > 0
    #       bchs = Branch.find_by_sql("SELECT clients.name as 'client_name', clients.indirect as 'client_indirect', 'client_indirect', route_histories.started_at as 'r_h_started',route_histories.finished_at as 'r_h_finished', route_histories.branch_id as 'r_h_branch_id',
    #         CONCAT(cleaners.first_name, ' ', cleaners.last_name) as 'cleaner_name', cleaners.phone as 'cleaner_phone',
    #         CONCAT(supervisors.first_name, ' ', supervisors.last_name) as 'supervisor_name', supervisors.phone as 'supervisor_phone',
    #         CONCAT(areamanagers.first_name, ' ', areamanagers.last_name) as 'areamanager_name', areamanagers.phone as 'areamanager_phone',
    #         CONCAT(assigned.first_name, ' ', assigned.last_name) as 'assigned_name',
    #         branches.* FROM branches 
    #         inner join route_histories on route_histories.branch_id = branches.id  
    #         LEFT JOIN clients ON clients.id = branches.client_id
    #         LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
    #         LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
    #         LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id
    #         left join users as assigned on assigned.id = route_histories.assigned_id WHERE route_histories.started_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}' AND route_histories.finished_at BETWEEN '#{start.beginning_of_day+(360*60)}' AND '#{start.end_of_day+(360*60)}'  AND branches.frequency LIKE '%#{start_n}%' AND branches.active =1 and branches.deleted = 0 and branches.id in (#{ids_b.map(&:inspect).join(', ')})")
  
    #       if bchs.length > 0 
    #         bchs.each do |bc|
    #           if chekins_data.include? bc.id
    #             with_checks[fecha].push(bc)
    #             branches_with << bc
    #           end
    #         end
    #       end
    #     end
        

        

        
      
    #   start = (start + 1.day)

    # end
    # # start_n =  params['start'].to_time
    # # finish_n =  params['finish'].to_time

    
    
    # sites = Branch.all.where(active: 1).where(deleted: 0).pluck(:id,:name)
    # clients = Client.all.where(active: 1).where(deleted: 0).pluck(:id,:name)
    
    # cleaners = Cleaner.all.where(role_id: 6).where(status: 1).where(deleted: 0).pluck(:id,:first_name,:last_name)
    # supervisors = Supervisor.all.where(role_id: 5).where(status: 1).where(deleted: 0).pluck(:id, :first_name,:last_name)

    # reponse = {
    #   'sites': sites,
    #   'without_checkins': without_checks,
    #   'with_checkins': with_checks,
    #   'clients': clients,
    #   'branches_with': branches_with.length,
    #   'branches_without': branches_without.length,
    #   'all': branches.length,
    #   'cleaners': cleaners,
    #   'supervisors': supervisors
    #   # 'checks': checks
    # }

    # render json: reponse
  end
  
  
  private
    def set_route_history
      @route_history = RouteHistory.find(params[:id])
    end

    def route_history_params
        params.permit(
            :user_id,
            :cleaner_id,
            :assigned_id,
            :supervisor_id,
            :area_manager_id,
            :scheduled_date,
            :branch_id,
            :route_schedule_id,
            :started_at,
            :finished_at,
            :status,
            :supervisor,
            :latitude_checkin,
            :longitude_checkin,
            :latitude_checkout,
            :longitude_checkout,
            :reassigned,
            :rescheduled,
            :delayed)
      end
end
