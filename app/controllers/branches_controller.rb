class BranchesController < ApplicationController
  before_action :authenticate_user!, except: [:notification_delayed, :notification_after, :notification_after_duration]
  before_action :set_branch, only: [:show, :update, :destroy,:active_services]

  def index
    @branches = Branch.all.where(deleted: 0)
    render json: @branches, include: [:client, :service_types, :cleaner, :area_manager, :supervisor, :extra_cleaners, :extra_area_managers, :extra_supervisors, :building_type]
  end

  def show
    render json: @branch, include: [:client, :service_types, :cleaner, :area_manager, :supervisor, :extra_cleaners, :extra_area_managers, :extra_supervisors, :building_type, comments: {include: [:user]}]
  end

  def create
    @branch = Branch.new(branch_params)

    if params['service_types']

      params['service_types'].each do |service_types|
        @branch.service_types << ServiceType.find(service_types)
      end
    end
    # Extra cleaners
    if params['extra_cleaners']
      params['extra_cleaners'].each do |extra_cleaners|
        @branch.extra_cleaners << ExtraCleaner.find(extra_cleaners)
      end
    end
    # Extra Area Managers
    if params['extra_area_managers']
      params['extra_area_managers'].each do |extra_area_managers|
        @branch.extra_area_managers << ExtraAreaManager.find(extra_area_managers)
      end
    end
    # Extra supervisors
    if params['extra_supervisors']
      params['extra_supervisors'].each do |extra_supervisors|
        @branch.extra_supervisors << ExtraSupervisor.find(extra_supervisors)
      end
    end



    if @branch.save
      render json: @branch, status: :created, location: @branch
    else
      render json: @branch.errors, status: :unprocessable_entity
    end
  end

  def update
    if @branch.update(branch_params)

      if params['service_types']
        params['service_types'].each do |service_types|
          @branch.service_types << ServiceType.find(service_types)
        end
      end

      # Extra cleaners
    if params['extra_cleaners']
      params['extra_cleaners'].each do |extra_cleaners|
        @branch.extra_cleaners << ExtraCleaner.find(extra_cleaners)
      end
    end
    # Extra Area Managers
    if params['extra_area_managers']
      params['extra_area_managers'].each do |extra_area_managers|
        @branch.extra_area_managers << ExtraAreaManager.find(extra_area_managers)
      end
    end
    # Extra supervisors
    if params['extra_supervisors']
      params['extra_supervisors'].each do |extra_supervisors|
        @branch.extra_supervisors << ExtraSupervisor.find(extra_supervisors)
      end
    end

      render json: @branch
    else
      render json: @branch.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @branch.update({deleted: 1})
      render json: true
    else
      render json: false
    end
  end

  def active_services
    render json: @branch, include: [:active_services]
  end
  def geofence

      user = @current_user
    latitude = params[:lat]
    longitude = params[:lng]


    day_number = DateTime.now.in_time_zone.strftime('%u')

    ids = []
    branches_ids = ""
    reassigned_ids =[]


    site_actives = Branch.all.where(cleaner_id: user.id).where("frequency like ?","%#{day_number}%" )

    reassigned = RouteHistory.all.where(user_id: user.id).where("(scheduled_date = '#{DateTime.now.strftime('%Y-%m-%d')}')").where(reassigned: 1).where(status: 0).order('id desc')
    # binding.pry
    reassigned.each do |rs|

      if RouteHistory.all.where('branch_id = ? ',rs.branch_id).where("scheduled_date = '#{rs.scheduled_date}' AND id > ?", rs.id).count == 0
        reassigned_ids << rs.branch_id
      end

    end
    branch_reassigned= Branch.all.where(id: reassigned_ids)


    if site_actives.length > 0 || reassigned.length > 0
      site_actives.each do |sa|
        ids << sa.id
      end

      branch_reassigned.each do |ra|
        ids << ra.id
      end


      ids.each do |id|
        branches_ids << id.to_s + ","
      end
      branches_today = branches_ids.chomp(",")


      nearme_query = "SELECT ((ACOS(SIN('#{latitude}' * PI() / 180) * SIN(latitude * PI() / 180) + COS('#{latitude}' * PI() / 180) * COS(latitude * PI() / 180) * COS(('#{longitude}' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS 'distance', branches.* FROM branches WHERE branches.id IN (#{branches_today}) HAVING distance < 0.15 ORDER BY distance ASC;"
      @branches = Branch.find_by_sql(nearme_query)

      render json: @branches
    else

      branches = 'No hay Branches'
      render json: branches
    end

  end

  def mygeofence
    # Get Params
    latitude = params[:lat]
    longitude = params[:lng]

    # Internal params
    day_number = DateTime.now.in_time_zone.strftime('%u')

    # Supervisor
    if @current_user.role_id <= 5
      route_histories = RouteHistory.all
        .where("(created_at BETWEEN '#{DateTime.now.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}')")
        .where(scheduled_date: nil)
        .where('reassigned IS NULL AND rescheduled IS NULL')
        .where(assigned_id: @current_user.id)
        .where(delayed: false)
        .order(id: :ASC)

      supervisor_branches = Branch.where("supervisor_id = #{@current_user.id} OR area_manager_id = #{@current_user.id}").pluck(:id)
      route_histories_cleaners = RouteHistory.all
        .where("(created_at BETWEEN '#{DateTime.now.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}')")
        .where(scheduled_date: nil)
        .where('reassigned IS NULL AND rescheduled IS NULL')
        .where(branch_id: supervisor_branches)
        .where("(supervisor IS NULL OR supervisor != 1)")
        .where(delayed: false)
        .order(id: :ASC)
    else #Cleaner
      # Get all the branches with a Route History
      route_histories = RouteHistory.all.where(assigned_id: @current_user.id)
        .where("(created_at BETWEEN '#{DateTime.now.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}')")
        .where(scheduled_date: nil)
        .where('reassigned IS NULL AND rescheduled IS NULL')
        .where(delayed: false).order(id: :ASC)
    end

   # WorkOrders - Applied for EVERYONE
    work_orders = WorkOrder.where(cleaner_id: @current_user.id).where(status: [5, 6, 7])
      .order(scheduled_date: :ASC)
      #.where("(scheduled_date BETWEEN '#{DateTime.now.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}') OR finished_date IS NULL")

    # Reassigned for today
    reassigned = RouteHistory.all.where(assigned_id: @current_user.id).where("scheduled_date = '#{DateTime.now.in_time_zone.strftime('%Y-%m-%d')}'")
      .where('reassigned = 1 OR rescheduled = 1').order(scheduled_date: :ASC)

    # Near me ID's not mine
    extra_branches_ids = "0"
    reassigned_ids_not = []
    # Get only reassigned ids
    reassigned.each do |r|
      extra_branches_ids = extra_branches_ids + ", " + r.branch_id.to_s
      reassigned_ids_not << r.branch_id
    end

    # Get only workorders ids
    work_orders.each do |wo|
      extra_branches_ids = extra_branches_ids + ", " + wo.branch_id.to_s
    end

    # Supervisor
    if @current_user.role_id == 5
      # Sites for today
      sites_active = Branch.all.active
        .joins("LEFT JOIN branches_extra_supervisors ON branches_extra_supervisors.branch_id = branches.id LEFT JOIN users ON users.id = branches_extra_supervisors.user_id")
        .where("supervisor_id = ? OR branches_extra_supervisors.user_id = ?", current_user.id, current_user.id)
        .where("frequency like ?", "%#{day_number}%")


    else # Cleaner
      # Sites for today
      sites_active = Branch.all.active
        .joins("LEFT JOIN branches_extra_cleaners ON branches_extra_cleaners.branch_id = branches.id LEFT JOIN users ON users.id = branches_extra_cleaners.user_id")
        .where("cleaner_id = ? OR branches_extra_cleaners.user_id = ?", current_user.id, current_user.id)
        .where("frequency like ?", "%#{day_number}%")
        .where.not(id: reassigned_ids_not)
    end

    if @current_user.role_id == 6
      day_number_yesterday = DateTime.yesterday.in_time_zone.strftime('%u')
      # Sites delayed finished
      sites_active_delayed_finished_ids = RouteHistory.all
        .where(assigned_id: @current_user.id)
        .where("(created_at BETWEEN '#{DateTime.yesterday.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}')  AND scheduled_date IS NULL").where('reassigned IS NULL AND rescheduled IS NULL')
        .where(delayed: true)
        .where("finished_at IS NOT NULL")
        .pluck(:branch_id)

      # Sites delayed
      sites_active_delayed = Branch.all
        .active
        .where(cleaner_id: @current_user.id)
        .where("frequency like ?", "%#{day_number_yesterday}%")
        .where.not(id: reassigned_ids_not)
        .where.not(id: sites_active_delayed_finished_ids)


      route_histories_delayed = RouteHistory.all
        .where(assigned_id: @current_user.id)
        .where("(created_at BETWEEN '#{DateTime.yesterday.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}')  AND scheduled_date IS NULL").where('reassigned IS NULL AND rescheduled IS NULL')
        .where(delayed: true)
        .where("finished_at IS NULL")
        .order(id: :ASC)
    end

    # Add join to extra clenaer/AM/supervisor to get all the branches inside my geofence
    if @current_user.role_id == 4
      extra_join = "LEFT JOIN branches_extra_area_managers ON branches_extra_area_managers.branch_id = branches.id LEFT JOIN users ON users.id = branches_extra_area_managers.user_id"
      extra_where = "branches_extra_area_managers.user_id = #{@current_user.id}"
    elsif @current_user.role_id == 5
      extra_join = "LEFT JOIN branches_extra_supervisors ON branches_extra_supervisors.branch_id = branches.id LEFT JOIN users ON users.id = branches_extra_supervisors.user_id"
      extra_where = "branches_extra_supervisors.user_id = #{@current_user.id}"
    else
      extra_join = "LEFT JOIN branches_extra_cleaners ON branches_extra_cleaners.branch_id = branches.id LEFT JOIN users ON users.id = branches_extra_cleaners.user_id"
      extra_where = "branches_extra_cleaners.user_id = #{@current_user.id}"
    end

    # Get all the branches inside my geofence
    nearme_query = "SELECT ((ACOS(SIN('#{latitude}' * PI() / 180) * SIN(latitude * PI() / 180) + COS('#{latitude}' * PI() / 180) * COS(latitude * PI() / 180) * COS(('#{longitude}' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS 'distance', branches.* FROM branches #{extra_join} WHERE (cleaner_id = #{@current_user.id} OR supervisor_id = #{@current_user.id} OR area_manager_id = #{@current_user.id}) OR branches.id IN(#{extra_branches_ids}) OR #{extra_where} HAVING distance < 0.15 ORDER BY distance ASC;"
    nearme_ids = Branch.find_by_sql(nearme_query).pluck(:id)


    # If supervisor, area manager or admin, show the closest sites without restrictions.
    if @current_user.role_id <= 5
      # Get closest sites, no matter whos assigned.
      managers_nearme_query = "SELECT ((ACOS(SIN('#{latitude}' * PI() / 180) * SIN(latitude * PI() / 180) + COS('#{latitude}' * PI() / 180) * COS(latitude * PI() / 180) * COS(('#{longitude}' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS 'distance', branches.* FROM branches HAVING distance < 1.00 ORDER BY distance ASC;"
      managers_nearme = Branch.find_by_sql(managers_nearme_query)
      managers_branches = Branch.all.active.where("supervisor_id = #{@current_user.id} OR area_manager_id = #{@current_user.id}")

      render json: {
        sites_active: ActiveModel::Serializer::CollectionSerializer.new(sites_active, nearme_ids: nearme_ids, route_histories: route_histories, current_user: @current_user, serializer: SupervisorActiveNearmeSerializer),
        sites_active_cleaners: ActiveModel::Serializer::CollectionSerializer.new(sites_active, nearme_ids: nearme_ids, route_histories: route_histories_cleaners, current_user: @current_user, serializer: SupervisorActiveNearmeSerializer),
        workorders: ActiveModel::Serializer::CollectionSerializer.new(work_orders, nearme_ids: nearme_ids, route_histories: route_histories, serializer: WorkOrdersNearmeSerializer),
        reassigned: ActiveModel::Serializer::CollectionSerializer.new(reassigned, nearme_ids: nearme_ids, route_histories: route_histories, serializer: ReassignedNearmeSerializer),
        nearme: ActiveModel::Serializer::CollectionSerializer.new(managers_nearme, nearme_ids: nearme_ids, route_histories: route_histories, serializer: ManagersNearmeSerializer),
        all: ActiveModel::Serializer::CollectionSerializer.new(managers_branches, nearme_ids: nearme_ids,route_histories: route_histories, serializer: ManagersBranchesSerializer)
      }
    else
      render json: {
        sites_delayed: [],
        sites_active: ActiveModel::Serializer::CollectionSerializer.new(sites_active, nearme_ids: nearme_ids, route_histories: route_histories, serializer: SitesActiveNearmeSerializer),
        workorders: ActiveModel::Serializer::CollectionSerializer.new(work_orders, nearme_ids: nearme_ids, route_histories: route_histories, serializer: WorkOrdersNearmeSerializer),
        reassigned: ActiveModel::Serializer::CollectionSerializer.new(reassigned, nearme_ids: nearme_ids, route_histories: route_histories, serializer: ReassignedNearmeSerializer)
      }
    end


  end


  def getbycleaner
    user = params['id']

    day_number = DateTime.now.in_time_zone.strftime('%u')

    rescheduled_ids=[]
    site_actives_ids=[]

    rescheduled = RouteHistory.all.where(user_id: user).where(rescheduled: 1).where(status: 0)

    #reasignados que no son site del current user (ignoro los reasignados de los site pertenecientes al current user)
    reassigned = RouteHistory.joins('INNER JOIN branches on branches.id = route_histories.branch_id').where("route_histories.scheduled_date = '#{DateTime.now.strftime('%Y-%m-%d')}' ").where('branches.cleaner_id <> ?',user).where('route_histories.user_id = ?',user).where('route_histories.reassigned = 1').where('route_histories.status = 0')

    rescheduled.each do |rd|
      site_actives_ids << rd.branch_id
    end

    reassigned.each do |ra|
      site_actives_ids << ra.branch_id
    end

    #sites del current user
    site_actives = Branch.all.where(cleaner_id: user).where("frequency like ?","%#{day_number}%" ).order('id desc')

    site_actives.each do |sa|
      rh_check = RouteHistory.all.where('branch_id = ? ',sa.id).where("scheduled_date = '#{DateTime.now.in_time_zone.strftime('%Y-%m-%d')}' AND reassigned = 1").order('id desc').limit(1);

      if rh_check.count == 0 || rh_check[0].user_id == user.to_i
        site_actives_ids << sa.id
      end
    end
    site_actives = Branch.all.where(id: site_actives_ids)

    sites_completes = RouteHistory.all.where(branch_id: site_actives_ids).where(status: 2).where(reassigned: nil).where("(updated_at BETWEEN '#{DateTime.now.in_time_zone.beginning_of_day}' AND '#{DateTime.now.in_time_zone.end_of_day}')")

    @response={
      "user": user,
      "sites_actives": site_actives,
      "completes": sites_completes

    }

    render json:@response

  end

  def getbycleanerinclude
    user = params['id']
    latitude = params['lat']
    longitude = params['lng']
    day_number = DateTime.now.in_time_zone.strftime('%u')

    ids = []
    branches_ids = ""
    @branches_resp = []
    site_actives = Branch.all.where(cleaner_id: user).where("frequency like ?","%#{day_number}%" )
    site_actives.each do |sa|
      ids << sa.id
      nearme_query = "SELECT ((ACOS(SIN('#{latitude}' * PI() / 180) * SIN(latitude * PI() / 180) + COS('#{latitude}' * PI() / 180) * COS(latitude * PI() / 180) * COS(('#{longitude}' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS 'distance', branches.* FROM branches WHERE branches.id = '#{sa.id}' HAVING distance < 0.15 ORDER BY distance ASC;"
      @branches = Branch.find_by_sql(nearme_query)
      authorized = false
      # binding.pry
      if @branches.count > 0 || (latitude == '0' && longitude == '0')
        authorized = true
      end
      @brn_aux = {
        branch: sa,
        authorized: authorized,
        client: Client.find(sa.client_id),
        histories: RouteHistory.where("branch_id = ? AND started_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}' AND status <> 0 AND status is not NULL AND supervisor is NULL ", sa.id)
      }
      @branches_resp << @brn_aux;
    end


    render json: @branches_resp

  end

  def getbysupervisorinclude
    user = params['id']
    latitude = params['lat']
    longitude = params['lng']
    day_number = DateTime.now.in_time_zone.strftime('%u')

    ids = []
    branches_ids = ""
    @branches_resp = []
    site_actives = Branch.all.where(supervisor_id: user)
    site_actives.each do |sa|
      ids << sa.id
      nearme_query = "SELECT ((ACOS(SIN('#{latitude}' * PI() / 180) * SIN(latitude * PI() / 180) + COS('#{latitude}' * PI() / 180) * COS(latitude * PI() / 180) * COS(('#{longitude}' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS 'distance', branches.* FROM branches WHERE branches.id = '#{sa.id}' HAVING distance < 0.15 ORDER BY distance ASC;"
      @branches = Branch.find_by_sql(nearme_query)
      authorized = false
      # binding.pry
      if @branches.count > 0 || (latitude == '0' && longitude == '0')
        authorized = true
      end
      @brn_aux = {
        branch: sa,
        authorized: authorized,
        client: Client.find(sa.client_id),
        histories: RouteHistory.where("branch_id = ? AND started_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}' AND status <> 0 AND status is not NULL AND supervisor = 1 ", sa.id)
      }
      @branches_resp << @brn_aux;
    end

    render json: @branches_resp

  end

  def getbysupervisor
    user = params['id']

    hoy = DateTime.now.in_time_zone.strftime('%A')
    dia = hoy.slice(0..2)

    day_number = case dia
      when "Mon" then "1"
      when "Tue" then "2"
      when "Wen" then "3"
      when "Thu" then "4"
      when "Fri" then "5"
      when "Sat" then "6"
      else "7"
    end

    site_actives = Branch.all.where(supervisor_id: user).where("frequency like ?","%#{day_number}%" )

    @response={
      "user": user,
      "sites_actives": site_actives

    }

    render json:@response

  end

  def notifications
    now = DateTime.now.in_time_zone.strftime("%H:%M")


    today = DateTime.now.in_time_zone.strftime('%A')
    day = today.slice(0..2)

    day_number = DateTime.now.in_time_zone.strftime('%u')

    # if now == "08:40"
    #   Branch.all.where("frequency like ?","%#{day_number}%" ).update_all(status: "waiting")
    # end

    ids = []

    max_time=[]

    tokens=[]

    site_actives = Branch.all.where("frequency like ?","%#{day_number}%" )


    users=[]
    site_actives.each do |sa|

       time = sa.max_time.to_time + 15*60
       max_time = time.strftime("%H:%M")
       ids << sa.cleaner_id
       if now == max_time #and sa.status != in prosess
        yes = "entro"
        users = User.all.where(id: ids)
       else
        yes = "no entro"
       end

    end



    # @response={
    #   "user": max_time
    # }
    render json: users
    # render json:@response



  end

  def getsupervisorall
    latitude = params[:lat]
    longitude = params[:lng]

    route_histories = RouteHistory.all.where(assigned_id: @current_user.id).where("(created_at BETWEEN '#{DateTime.now.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}') AND scheduled_date IS NULL").where('reassigned IS NULL AND rescheduled IS NULL').order(id: :ASC)

    nearme_query = "SELECT ((ACOS(SIN('#{latitude}' * PI() / 180) * SIN(latitude * PI() / 180) + COS('#{latitude}' * PI() / 180) * COS(latitude * PI() / 180) * COS(('#{longitude}' - longitude) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS 'distance', branches.* FROM branches WHERE (supervisor_id = #{@current_user.id}) HAVING distance < 0.15 ORDER BY distance ASC;"
    nearme_ids = Branch.find_by_sql(nearme_query).pluck(:id)

    managers_branches = Branch.all.where(supervisor_id: @current_user.id)

    render json:{
      sites: ActiveModel::Serializer::CollectionSerializer.new(managers_branches, nearme_ids: nearme_ids,route_histories: route_histories, serializer: ManagersBranchesSerializer)

    }

  end



  def notification_after

    user_id = params[:id]

    #now = DateTime.now.in_time_zone.strftime("%H:%M")

    now_hour = DateTime.now.in_time_zone.strftime("%H")
    now_minutes = DateTime.now.in_time_zone.strftime("%M")

    today = DateTime.now.in_time_zone.strftime('%A')
    day = today.slice(0..2)

    day_number = DateTime.now.in_time_zone.strftime('%u')

    branches_ids = []
    branch_time = []

    site_actives = Branch.all.where(cleaner_id: user_id).where("frequency like ?","%#{day_number}%" ).order('max_time ASC')
    cleaner_id=[]
    area_manager_id=[]
    supervisor_id=[]

    site_actives.each do |sa|
      cleaner_id << sa.cleaner_id
      area_manager_id << sa.area_manager_id
      supervisor_id << sa.supervisor_id
      branches_ids << sa.id

    end

    histories = RouteHistory.all.where(branch_id: branches_ids).where("(created_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}')")

    cleaner = User.all.where(id: cleaner_id)
    area_manager = User.all.where(id: area_manager_id)
    supervisor = User.all.where(id: supervisor_id)

    if histories.length  > 0 and histories.length < site_actives.length
      ultimo = histories.last
      last = ultimo.finished_at + 5*60
      last_time =  last.to_time
      history = last.strftime("%H:%M")

      max_time_history = last + 10*60
      max_time_history_hour = max_time_history.strftime("%H")
      max_time_history_min = max_time_history.strftime("%M")
      max_time_history_to_time = "#{max_time_history_hour}.#{max_time_history_min}".to_f

      now_to_time = "#{now_hour}.#{now_minutes}".to_f

      first_hour = last_time.strftime("%H")
      first_min = last_time.strftime("%M")
      first_number = "#{first_hour}.#{first_min}".to_f

     if now_to_time > first_number and now_to_time < max_time_history_to_time

        @response={
          "status": "notification",
          "cleaner": cleaner,
          "area_manager": area_manager,
          "supervisor": supervisor
        }
      else
        @response={
          "status": "nothing",
          "cleaner": nil,
          "area_manager": nil,
          "supervisor": nil
        }
      end
    end


    #render json: users
     render json:@response

  end

  def notification_after_duration

    user_id = params[:id]

    now = DateTime.now.in_time_zone.strftime("%H:%M")


    today = DateTime.now.in_time_zone.strftime('%A')
    day = today.slice(0..2)

    day_number = DateTime.now.in_time_zone.strftime('%u')

    branches_ids = []
    branch_time = []

    site_actives = Branch.all.where(cleaner_id: user_id).where("frequency like ?","%#{day_number}%" ).order('max_time ASC')
    cleaner_id=[]
    area_manager_id=[]
    supervisor_id=[]

    site_actives.each do |sa|
      cleaner_id << sa.cleaner_id
      area_manager_id << sa.area_manager_id
      supervisor_id << sa.supervisor_id
      branches_ids << sa.id

    end

    histories = RouteHistory.all.where(branch_id: branches_ids).where("(created_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}')").order('created_at ASC')

    cleaner = User.all.where(id: cleaner_id)
    area_manager = User.all.where(id: area_manager_id)
    supervisor = User.all.where(id: supervisor_id)


    if histories.length  > 0 and histories.length < site_actives.length
      ultimo = histories.last

      branch_last = Branch.all.where(id: ultimo.branch_id)
      duration= branch_last[0].duration_max
      last = ultimo.started_at + duration*60
      history = last.strftime("%H:%M")

     if now.to_time >= history.to_time and now.to_time < history.to_time + 10*60

        @response={
          "status": "notification",
          "cleaner": cleaner,
          "area_manager": area_manager,
          "supervisor": supervisor
        }
      else
        @response={
          "status": "nothing",
          "cleaner": nil,
          "area_manager": nil,
          "supervisor": nil
        }
      end
    end
    # @response={
    #   "branch_last": last
    # }

    #render json: users
     render json:@response

  end

  def getnewbranches
    user = @current_user

    reassigned = RouteHistory.all.where(user_id: user.id).where("(scheduled_date = '#{DateTime.now.in_time_zone.strftime('%Y-%m-%d')}')").where(reassigned: 1).where(status: 0)

    rescheduled = RouteHistory.all.where(user_id: user.id).where(rescheduled: 1).where(status: 0)

    work_order= WorkOrder.all.where(cleaner_id: user.id).where(status: 5)

    reassigned_ids =[]
    rescheduled_ids= []
    work_order_ids=[]

    reassigned.each do |rs|
      if RouteHistory.all.where('branch_id = ?',rs.branch_id).where("scheduled_date = '#{rs.scheduled_date}' AND id > ?",rs.id).count == 0 && Branch.where('id = ? AND cleaner_id != ?', rs.branch_id,user.id).count > 0
        reassigned_ids << rs.branch_id
      end

    end

    work_order.each do |rd|
      work_order_ids << rd.branch_id
    end

    rescheduled.each do |rd|
      rescheduled_ids << rd.branch_id
    end

    branch_reassigned= Branch.all.where(id: reassigned_ids)

    branch_rescheduled= Branch.all.where(id: rescheduled_ids)

    branch_order= Branch.all.where(id: work_order_ids)

    # reassigned_completes = RouteHistory.all.where(branch_id: reassigned_ids).where(status: 2).where("(created_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}')")

    # rescheduled_completes = RouteHistory.all.where(branch_id: rescheduled_ids).where(status: 2).where("(created_at BETWEEN '#{DateTime.now.beginning_of_day}' AND '#{DateTime.now.end_of_day}')")

    @response={
      "reasignados": branch_reassigned,
      "reprogramados": branch_rescheduled,
      "works": branch_order,
      "order": work_order
    }

    render json:@response
  end

  def paginate
    user = @current_user
    role = Role.find(user.role_id)
    per_page_limit = 50
    if role.id == 5 || role.id == 4
      if params['filter'].nil?
        @branches = Branch.where(active: 1).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).paginate(:page => params[:page], :per_page => per_page_limit)
      else
        if params['filter_by'] == 'client'
          @branches = Branch.all.where(client_id: params['client_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'customer'
          @branches = Branch.all.where(client_id: params['customer_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'customer_for_client'
           @branches = Branch.all.where(client_id: params['customer_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'cleaner'
          @branches = Branch.all.where(cleaner_id: params['cleaner_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'state'
          @branches = Branch.all.where(state: params['state']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'city'
          @branches = Branch.all.where(city: params['city']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
        if params['filter_by'] == 'address'
          @branches = Branch.all.where("address like ?","%#{params['address']}%" ).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
        if params['filter_by'] == 'site_name'
          @branches = Branch.all.where("name like ?","%#{params['site_name']}%" ).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
        if params['filter_by'] == 'site_code'
          @branches = Branch.all.where("site_code like ?","%#{params['site_code']}%" ).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
      end
    else

      if params['filter'].nil?
        @branches = Branch.where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
      else
        if params['filter_by'] == 'client'
          @branches = Branch.all.where(client_id: params['client_id']).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'customer'
          @branches = Branch.all.where(client_id: params['customer_id']).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'customer_for_client'
           @branches = Branch.all.where(client_id: params['customer_id']).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'cleaner'
          @branches = Branch.all.where(cleaner_id: params['cleaner_id']).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'supervisor'
          @branches = Branch.all.where(supervisor_id: params['supervisor_id']).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'state'
          @branches = Branch.all.where(state: params['state']).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end

        if params['filter_by'] == 'city'
          @branches = Branch.all.where(city: params['city']).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
        if params['filter_by'] == 'address'
          @branches = Branch.all.where("address like ?","%#{params['address']}%" ).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
        if params['filter_by'] == 'site_name'
          @branches = Branch.all.where("name like ?","%#{params['site_name']}%" ).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
        if params['filter_by'] == 'site_code'
          @branches = Branch.all.where("site_code like ?","%#{params['site_code']}%" ).where(active: 1).paginate(:page => params[:page], :per_page => per_page_limit)
        end
      end
    end
    render json: [@branches, :page => params[:page]], include: [:client, :service_types, :cleaner, :area_manager, :supervisor, :building_type]
  end

  def inactive
    user = @current_user
    role = Role.find(user.role_id)

    if role.id == 5 || role.id == 4
      if params['filter'].nil?
        @branches = Branch.where(active: 0).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).paginate(:page => params[:page], :per_page => 50)
      else
        if params['filter_by'] == 'client'
          @branches = Branch.all.where(client_id: params['client_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'customer'
          @branches = Branch.all.where(client_id: params['customer_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'customer_for_client'
           @branches = Branch.all.where(client_id: params['customer_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'cleaner'
          @branches = Branch.all.where(cleaner_id: params['cleaner_id']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'state'
          @branches = Branch.all.where(state: params['state']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'city'
          @branches = Branch.all.where(city: params['city']).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
        if params['filter_by'] == 'address'
          @branches = Branch.all.where("address like ?","%#{params['address']}%" ).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
        if params['filter_by'] == 'site_name'
          @branches = Branch.all.where("name like ?","%#{params['site_name']}%" ).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
        if params['filter_by'] == 'site_code'
          @branches = Branch.all.where("site_code like ?","%#{params['site_code']}%" ).where('supervisor_id = ? or area_manager_id = ?', user.id, user.id).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
      end
    else

      if params['filter'].nil?
        @branches = Branch.where(active: 0).paginate(:page => params[:page], :per_page => 50)
      else
        if params['filter_by'] == 'client'
          @branches = Branch.all.where(client_id: params['client_id']).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'customer'
          @branches = Branch.all.where(client_id: params['customer_id']).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'customer_for_client'
           @branches = Branch.all.where(client_id: params['customer_id']).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'cleaner'
          @branches = Branch.all.where(cleaner_id: params['cleaner_id']).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'state'
          @branches = Branch.all.where(state: params['state']).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end

        if params['filter_by'] == 'city'
          @branches = Branch.all.where(city: params['city']).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
        if params['filter_by'] == 'address'
          @branches = Branch.all.where("address like ?","%#{params['address']}%" ).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
        if params['filter_by'] == 'site_name'
          @branches = Branch.all.where("name like ?","%#{params['site_name']}%" ).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
        if params['filter_by'] == 'site_code'
          @branches = Branch.all.where("site_code like ?","%#{params['site_code']}%" ).where(active: 0).paginate(:page => params[:page], :per_page => 50)
        end
      end
    end

    render json: [@branches, :page => params[:page]], include: [:client, :service_types, :cleaner, :area_manager, :supervisor]
  end

  def states
    @states = Branch.select('state').where('state != ""').order('state asc').distinct

    render json: @states
  end

  def cities
    @cities = Branch.select('city').where('city != ""').order('city asc').distinct

    render json: @cities
  end

  def pendding
    id = params[:id]

    histories = RouteHistory.all.where(branch_id: id).where(reassigned: 1).where(status: 0).order('created_at ASC')

    historiesChekin = RouteHistory.all.where(branch_id: id).where(reassigned: nil).where(status: 1).order('created_at ASC')

    @history ={
      "history": histories.last,
      "pendding": historiesChekin.last
    }

    render json: @history

  end


  def requestUpdateLocations
    lat = params['new_lat']
    lng = params['new_lng']
    user = @current_user
    branch = Branch.find(params[:id])
    branch.update({update_location_lat: lat, update_location_lng: lng, user_request_update: user.id, status_request_update: 1})

    render json: branch
  end

  def showRequestList
    query_request = 'SELECT clients.name as "client_name", clients.indirect as "client_indirect",
    CONCAT(users.first_name, " ", users.last_name) as "users_name",
    branches.* FROM branches
    LEFT JOIN clients ON clients.id = branches.client_id
    LEFT JOIN users as users ON users.id = branches.user_request_update WHERE (status_request_update = 1)'

    branches = Branch.find_by_sql(query_request)
    render json: branches
  end

  def active
    branches = Branch.where(active: 1).where(deleted: 0)

    render json: branches,include: [:client, :cleaner, :area_manager, :supervisor]
  end

  def export_active_sites
    sites_query = 'SELECT clients.name as "client_name", clients.indirect as "client_indirect",
                  CONCAT(cleaners.first_name, " ", cleaners.last_name) as "cleaner_name", cleaners.phone as "cleaner_phone",
                  CONCAT(supervisors.first_name, " ", supervisors.last_name) as "supervisor_name", supervisors.phone as "supervisor_phone",
                  CONCAT(areamanagers.first_name, " ", areamanagers.last_name) as "areamanager_name", areamanagers.phone as "areamanager_phone",
                  branches.* FROM branches
                  LEFT JOIN clients ON clients.id = branches.client_id
                  LEFT JOIN users as cleaners ON cleaners.id = branches.cleaner_id
                  LEFT JOIN users as supervisors ON supervisors.id = branches.supervisor_id
                  LEFT JOIN users as areamanagers ON areamanagers.id = branches.area_manager_id WHERE branches.active = 1;'
    sites = Branch.find_by_sql(sites_query)
    render json: sites, include: []
  end
  def direct
    clients_ids =  Branch.all.where(active: 0).pluck(:client_id)
    clientes = Client.all.where(id: clients_ids).where(indirect: 0)

    render json: clientes
  end
  def indirect
    clients_ids =  Branch.all.where(active: 0).pluck(:client_id)
    clientes = Client.all.where(id: clients_ids).where(indirect: 1)

    render json: clientes
  end
  def getbysitecode
    @branches = Branch.all.where("site_code like ?","%#{params['site_code']}%" )
    render json: @branches
  end
  private
    def set_branch
      @branch = Branch.find(params[:id])
    end

    def branch_params
      params.permit(
        :client_id,
        :supervisor_id,
        :area_manager_id,
        :cleaner_id,
        :frequency,
        :weekly_frequency,
        :name,
        :address,
        :address2,
        :state,
        :city,
        :zipcode,
        :phone,
        :email,
        :contact_name,
        :latitude,
        :longitude,
        :update_location_lat,
        :update_location_lng,
        :user_request_update,
        :status_request_update,
        :region,
        :max_time,
        :duration_min,
        :duration_max,
        :site_type,
        :site_code,
        :cleanable_area,
        :status,
        :active,
        :muted,
        :subregion,
        :time_per_cleaning,
        :start_time,
        :end_time,
        :route_option,
        :weekly_payout,
        :company_payroll,
        :type_of_contract,
        :building_type_id,
        service_types: [:id],
        extra_cleaners: [:id],
        extra_area_managers: [:id],
        extra_supervisors: [:id]
        )
    end
end
