class RolesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_role, only: [:show, :update, :destroy]

  def index
    @roles = Role.all

    render json: @roles, include: [:users]
  end

  def show
    render json: @role, include: [:users]
  end

  def create
    @role = Role.new(role_params)

    if @role.save
      render json: @role, status: :created
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  def update
    if @role.update(role_params)
      render json: @role
    else
      render json: @role.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @role.destroy
      render json: true
    else
      render json: false
    end
  end

  private
    def set_role
      @role = Role.find(params[:id])
    end

    #Aqui debe estar require user? lo tomaria explicitamente al autenticarse en la linea 2?
    def role_params
      params.permit(:name)
    end
end
