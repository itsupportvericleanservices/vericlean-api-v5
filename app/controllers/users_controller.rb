class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:user_for_today,:token_notification]
  before_action :set_user, only: [:show, :update, :destroy]

  def index
    @users = User.all.where(deleted: 0)

    render json: @users, include: [:role, :client]
  end

  def show
   
    render json: @user, include: [:role, :client]
 
  end

  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @user.update({deleted: 1,status: 0})
      render json: true
    else
      render json: false
    end
  end

  def token_notification

    user = params[:id]

    token = User.all.where(id: user)

    @response ={"token": token[0].device_token}

    render json:@response
  end

  def getbyfrecuency
    day_number = params['f'];
    date = params['date'].to_time;
    @cleaners = Cleaner.joins("RIGHT JOIN branches ON branches.cleaner_id = users.id").where("branches.frequency like ?","%#{day_number}%").group('users.id');
  #binding.pry
    @resp_cleaners = []
    today = Time.now.in_time_zone.end_of_day.strftime("%H:%M:%S")
    @cleaners.each do |clr|
      min_max_time = today;
      warning = true;
      
      @branches = Branch.all.where("cleaner_id = ? and frequency like ?",clr.id,"%#{day_number}%" );
      @brn_aux = []
      @branches.each do |brn|
        # binding.pry
        if !brn.max_time.nil? && min_max_time > brn.max_time.strftime("%H:%M:%S")
           
          min_max_time = brn.max_time.strftime("%H:%M:%S")
        end
        
        route_histories = RouteHistory.where('branch_id = ?', brn.id).where("started_at BETWEEN '#{date.beginning_of_day+(360*60)}' AND '#{date.end_of_day+(360*60)}' AND supervisor is NULL");
        route_histories_status = RouteHistory.where('branch_id = ?', brn.id).where("(created_at BETWEEN '#{date.beginning_of_day+(360*60)}' AND '#{date.end_of_day+(360*60)}') AND reassigned = 1").count;
        
       #binding.pry

        
        if route_histories.length > 0 
          if !route_histories[0].started_at.nil? 
            warning = false;
            
          end
          
        end

        if route_histories_status == 0
          @branches_arry = {
          
          branch: {
            id: brn.id,
            client: Client.find(brn.client_id),
            client_id: brn.client_id,
            frequency: brn.frequency,
            weekly_frequency: brn.weekly_frequency,
            name: brn.name,
            address: brn.address,
            address2: brn.address2,
            state: brn.state,
            city: brn.city,
            zipcode: brn.zipcode,
            phone: brn.phone,
            email: brn.email,
            latitude: brn.latitude,
            longitude: brn.longitude,
            created_at: brn.created_at,
            updated_at: brn.updated_at,
            max_time: brn.max_time,
            site_type: brn.site_type,
            site_code: brn.site_code,
            status: brn.status,
            cleaner_id: brn.cleaner_id,
            area_manager_id: brn.area_manager_id,
            supervisor_id: brn.supervisor_id,
            history: route_histories
          }
         
        }

        
      
        @brn_aux << @branches_arry;
        end
        
        
      end #branches
      ######################
      @rh_reassigned = RouteHistory.joins(:branch).where("route_histories.user_id = ? AND (route_histories.created_at BETWEEN '#{date.beginning_of_day}' AND '#{date.end_of_day}') AND route_histories.reassigned = ?",clr.id, 1).select('branches.* , route_histories.id as rh_id');
      # @brn_aux = []
      @rh_reassigned.each do |brn|
        # binding.pry
        if !brn.max_time.nil? && min_max_time > brn.max_time.strftime("%H:%M:%S")
          # binding.pry;
          min_max_time = brn.max_time.strftime("%H:%M:%S")
        end
        
        route_histories = RouteHistory.where('branch_id = ?', brn.id).where("created_at BETWEEN '#{date.beginning_of_day}' AND '#{date.end_of_day}' AND supervisor is NULL");
        route_histories_status = RouteHistory.where('branch_id = ?', brn.id).where("(created_at BETWEEN '#{date.beginning_of_day}' AND '#{date.end_of_day}') AND reassigned = 1 AND id > ?", brn.rh_id).count;
        
        # binding.pry

        
        if route_histories.length > 0 
          if !route_histories[0].started_at.nil?
            warning = false;
            
          end
          
        end

        if route_histories_status == 0
          @branches_arry = {
          
          branch: {
            id: brn.id,
            client_id: brn.client_id,
            frequency: brn.frequency,
            weekly_frequency: brn.weekly_frequency,
            name: brn.name,
            address: brn.address,
            address2: brn.address2,
            state: brn.state,
            city: brn.city,
            zipcode: brn.zipcode,
            phone: brn.phone,
            email: brn.email,
            latitude: brn.latitude,
            longitude: brn.longitude,
            created_at: brn.created_at,
            updated_at: brn.updated_at,
            max_time: brn.max_time,
            site_type: brn.site_type,
            site_code: brn.site_code,
            status: brn.status,
            cleaner_id: brn.cleaner_id,
            area_manager_id: brn.area_manager_id,
            supervisor_id: brn.supervisor_id,
            history: route_histories
          }
         
        }

        
      
        @brn_aux << @branches_arry;
        end
        
        
      end #branches
#######################
      # binding.pry
      if min_max_time == today
        warning = false;
      end
      if @brn_aux.length  > 0   
        @resp_cleaners << {
          cleaner: clr,
          branches: @brn_aux,
          min_time: min_max_time,
          warning: warning,
          rh_reassigned: @rh_reassigned
        } 
      end
      

    end
    
    render json: @resp_cleaners

  end

  def user_for_today
    day_number = DateTime.now.in_time_zone.strftime('%u')
    users_ids = Branch.all.where.not(cleaner_id: nil).where.not(cleaner_id: 0).where("frequency like ?", "%#{day_number}%").group(:cleaner_id).pluck(:cleaner_id)
    render json: users_ids
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.permit(
        :username,
        :email,
        :password,
        :password_confirmation,
        :first_name,
        :last_name,
        :role_id,
        :client_id,
        :phone,
        :address,
        :status,
        :device_token)
    end
end
