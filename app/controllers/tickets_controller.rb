class TicketsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_tickets, only: [:show, :update, :destroy, :dd_comment]
  
  def index
    @tickets = Ticket.all
  
    render json: @tickets, include: {branch:{include:[:client]},comments:{include:[:user]}, work_orders:{include:[]}}
  end
  
  def show
    render json: @ticket, include: {branch:{include:[:client]},comments:{include:[:user]}, work_orders:{include:[]}}
  end

  def create
    @ticket = Ticket.new(ticket_params)
    @branch = Branch.find(ticket_params['branch_id']);
    @ticket.work_orders << WorkOrder.new({
                                          user_id: ticket_params['user_id'],
                                          branch_id: ticket_params['branch_id'],
                                          client_id: @branch['client_id'],
                                          requested_date: DateTime.current(),
                                          due_date: ticket_params['due_date'],
                                          request_contact: ticket_params['request_contact'],
                                          request_contact_email: ticket_params['request_contact_email'],
                                          request_phone: ticket_params['request_contact_phone'],
                                          task_description: ticket_params['description'],
                                          status: 0,
                                          main_type: 'complain',
                                          priority: 1

                                        })
    
    if @ticket.save
      render json: @ticket, status: :created, location: @ticket
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  def update
    if @ticket.update(ticket_params)
      render json: @ticket
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @ticket.destroy
      render json: true
    else
      render json: false
    end
  end
  def add_comment
    @user = current_user
    # binding.pry
    @ticket = Ticket.find(params['id'])
    @ticket.comments << Comment.new({user_id: @user['id'], comment: params['comment']})
    if @ticket.save
      render json: @ticket, status: :created, location: @ticket
    else
      render json: @ticket.errors, status: :unprocessable_entity
    end
  end
  private
    def set_tickets
      @ticket = Ticket.find(params[:id])
    end

    def ticket_params
      params.permit(
        :user_id,
        :branch_id,
        :request_contact,
        :request_contact_email,
        :request_contact_phone,
        :event_date,
        :due_date,
        :short_description,
        :description,
        :status,
        comments:[])
    end
end
  