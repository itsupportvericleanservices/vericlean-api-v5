class NotificationsController < ApplicationController
  before_action :authenticate_user!, except: [:delayed]
  before_action :set_notification, only: [:show, :update, :destroy]

  def index
    if current_user.role_id == 1
      @notifications = Notification.all.order(created_at: :DESC).limit(300)
    else
      @notifications = current_user.notifications.order(created_at: :DESC).limit(100)
    end 

    render json: @notifications, include: [:user]
  end

  def show
    render json: @notification, include: [:user]
  end

  def create
    @notification = Notification.new(notification_params)

    if @notification.save
      render json: @notification, status: :created
    else
      render json: @notification.errors, status: :unprocessable_entity
    end
  end

  def update
    if @notification.update(notification_params)
      render json: @notification
    else
      render json: @notification.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @notification.destroy
  end

  def delayed
    user_id = params[:id]

    now_hour = DateTime.now.in_time_zone.strftime("%H")
    now_minutes = DateTime.now.in_time_zone.strftime("%M")
    day_number = DateTime.now.in_time_zone.strftime('%u')

    branches_ids = []
    branch_time = []
    cleaner_id = []
    area_manager_id = []
    supervisor_id = []

    sites_active = Branch.all.where(cleaner_id: user_id).where("frequency like ?", "%#{day_number}%").where(active: 1).where(muted: 0).order('max_time ASC')
    if !sites_active.empty?
      sites_active.each do |sa|
        cleaner_id << sa.cleaner_id
        area_manager_id << sa.area_manager_id
        supervisor_id << sa.supervisor_id
        branches_ids << sa.id

        # 20:00 to 21:00 FIX
        if sa.max_time.strftime("%H") == "20"
          branch_time << sa.max_time.to_time + 59*60
        else
          branch_time << sa.max_time.to_time + 1*60
        end
      end

      histories = RouteHistory.all.where(user_id: user_id).where(branch_id: branches_ids).where("(created_at BETWEEN '#{DateTime.now.in_time_zone.beginning_of_day+(360*60)}' AND '#{DateTime.now.in_time_zone.end_of_day+(360*60)}')")

      cleaner = User.find(cleaner_id)
      area_manager = User.find(area_manager_id)
      supervisor = User.find(supervisor_id)

      now_to_time = "#{now_hour}.#{now_minutes}".to_f

      first_hour = branch_time[0].strftime("%H")
      first_min = branch_time[0].strftime("%M")
      first_number = "#{first_hour}.#{first_min}".to_f 


      if now_to_time > first_number 
        if histories.length == 0
          response = {
            "status": "notification",
            "cleaner": cleaner,
            "area_manager": area_manager,
            "supervisor": supervisor
          }
        else
          response = {
            "status": "empty",
            "cleaner": nil,
            "area_manager": nil,
            "supervisor": nil
          }
        end
      end

      # Render Response
      render json: response
    else
      render json: {"status": "empty", "message": "no notifications"}
    end  

  end

  private
    def set_notification
      @notification = Notification.find(params[:id])
    end

    def notification_params
      params.permit(
        :user_id,
        :success,
        :message,
        :sent_at,
        :device_token,
        :full_response
      )
    end
end
