class PhotosController < ApplicationController
  before_action :authenticate_user!
  before_action :set_photo, only: [:show, :update, :destroy]

  def index
    @photos = Photo.all

    render json: @photos, include: [:user, :service]
  end

  def show
    render json: [@photo, file_url: url_for(@photo_storage)], include: [:user, :service]
  end

  def create
    @photo = Photo.new(photos_params)
    @photo.images.attach(photos_attach_params[:images])
 
    if @photo.save
      render json: @photo, status: :created
    else
      render json: @photo.errors, status: :unprocessable_entity
    end
  end

  def update
    if @photo.update(photos_params)
      render json: @photo
    else
      render json: @photo.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @photo.destroy
    @photo_storage.purge
  end

  private
    def set_photo
      @photo = Photo.find(params[:id])
      @photo_storage = ActiveStorage::Attachment.find(params[:id])
      #@photo_storages = ActiveStorage::Attachment.all // buscar metodo para traerme todas las imagenes
      #@storage = ActiveStorage::Blob.find(params[:id]) // testear este metodo
    end

    def photos_params
      params.permit(
        :user_id, 
        :service_id)
    end

    def photos_attach_params
      params.permit(images: [])
    end
end
