class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_comment, only: [:show, :update, :destroy]

  def index
    @comments = Comment.all

    render json: @comments, include: [:user, :service]
  end

  def show
    render json: @comment, include: [:user, :service]
  end

  def create
    @comment = Comment.new(comments_params)

    if @comment.save
      render json: @comment, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def update
    if @comment.update(comments_params)
      render json: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @comment.destroy
  end

  private
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def comments_params
      params.permit(
          :user_id,
          :comment,
          :commentable_id,
          :commentable_type)
    end
end
