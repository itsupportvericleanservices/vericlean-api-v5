class EmailsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_email, only: [:show, :update, :destroy]

  def index
    @emails = Email.all

    render json: @emails, include: [:user, :work_order]
  end

  def show
    render json: @email, include: [:user, :work_order]
  end

  def create
    @email = Email.new(email_params)
   
    if @email.save
      render json: @email, status: :created
    else
      render json: @email.errors, status: :unprocessable_entity
    end
  end

  def update
    if @email.update(email_params)
      render json: @email
    else
      render json: @email.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @email.destroy
    @email_storage.purge
  end

  private
    def set_email
      @email = Email.find(params[:id])
     
    end

    def email_params
      params.permit(
        :user_id, 
        :work_order_id,
        :to,
        :subject,
        :body)
    end

    
end
