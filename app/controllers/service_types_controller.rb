class ServiceTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_service_type, only: [:show, :update, :destroy]

  def index
    @service_types = ServiceType.all

    render json: @service_types, include: [:branches]
  end

  def show
    render json: @service_type, include: [:branches]
  end

  def create
    @service_type = ServiceType.new(service_types_params)

    if @service_type.save
      render json: @service_type, status: :created
    else
      render json: @service_type.errors, status: :unprocessable_entity
    end
  end

  def update
    if @service_type.update(service_types_params)
      render json: @service_type
    else
      render json: @service_type.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @service_type.destroy
      render json: true
    else
      render json: false
    end
  end

  private
    def set_service_type
      @service_type = ServiceType.find(params[:id])
    end

    def service_types_params
      params.permit(
          :name,
          :min_time,
          :max_time)
    end
end
