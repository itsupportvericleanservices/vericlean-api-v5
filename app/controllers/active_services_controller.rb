class ActiveServicesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_active_service, only: [:show, :update, :destroy]
  
    def index
      @active_services = ActiveService.all
  
      render json: @active_services, include: [:branch, :service_types] #verificar progreso
    end
  
    def show
      render json: @active_service, include: [:branch, :service_types] #verificar progreso
    end
  
    def create
    @active_service = ActiveServices.new(active_services_params)
  
      if @active_service.save
        render json: @active_service, status: :created
      else
        render json: @active_service.errors, status: :unprocessable_entity
      end
    end
  
    def update
      if @active_service.update(active_services_params)
        render json: @active_service
      else
        render json: @active_service.errors, status: :unprocessable_entity
      end
    end
  
    def destroy
      @active_service.destroy
    end
  
    private
      def set_active_service
        @active_service = ActiveService.find(params[:id])
      end
  
      def active_services_params
        params.permit(
          :status, 
          :started_at, 
          :finished_at)
      end
  end
  