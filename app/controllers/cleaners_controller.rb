class CleanersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_cleaner, only: [:show, :update, :destroy]
  
  def index
    @cleaners = Cleaner.all.order('first_name asc')
  
     render json: @cleaners, include: [:role]
  end
  
  def show
    render json: @cleaner, include: [:role]
  end

  def create
    @cleaner = Cleaner.new(cleaner_params)

    if @cleaner.save
      render json: @cleaner, status: :created, location: @cleaner
    else
      render json: @cleaner.errors, status: :unprocessable_entity
    end
  end

  def update
    if @cleaner.update(cleaner_params)
      render json: @cleaner
    else
      render json: @cleaner.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @cleaner.update({deleted: 1,status: 0})
      render json: true
    else
      render json: false
    end
  end

  private
    def set_cleaner
      @cleaner = Cleaner.find(params[:id])
    end

    def cleaner_params
      params.permit(
        :username,
        :email,
        :password,
        :password_confirmation,
        :first_name,
        :last_name,
        :role_id,
        :client_id,
        :phone,
        :address,
        :status,
        :supervisor,
        :area_manager,
        :secundary_cleaner)
    end
end
  