class ClientsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_client, only: [:show, :update, :destroy]

  def index
    @clients = Client.all.where(deleted: 0)

    render json: @clients, include: [:branches]
  end

  def show
    render json: @client, include: [:branches]
  end

  def create
    @client = Client.new(client_params)

    if @client.save
      render json: @client, status: :created
    else
      render json: @client.errors, status: :unprocessable_entity
    end
  end

  def update
    if @client.update(client_params)

      #desactive todos los branches asociados.
      if params['active'] == '0' || params['active'] == 0
        Branch.where(client_id: params['id']).update_all(active: 0)
      end

      if params['active'] == '1' || params['active'] == 1
        #active todos los branches asociados.

        if params['active_sites'] == 'all'
          Branch.where(client_id: params['id']).update_all(active: 1)
        else
          #solo actualice los sites que se necesitan
          params['branches_id'].each do |branch|
            Branch.where(client_id: params['id']).where(id: branch).update_all(active: 1)
          end
        end
      end

      render json: @client, include: [:branches]
    else
    render json: @client.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @client.update({deleted: 1})
      render json: true
    else
      render json: false
    end
  end

  def active
    @clients = Client.where(active: 1).where(deleted: 0)
    render json: @clients, include: [:branches]
  end

  def inactive
    @clients = Client.where(active: 0).where(deleted: 0)
    render json: @clients, include: [:branches]
  end

  def sites
    @branches = Branch.where(client_id: params['id'])
    render json: @branches
  end

  def status
    @clients = Client.all.where(active: params['status']).where(indirect: 0).order('name asc')
    render json: @clients
  end

  def customer_status
    @clients = Client.all.where(active: params['status']).where(indirect: 1).order('name asc')
    render json: @clients
  end

  def customer_for_client
    @clients = Client.all.where(indirect_id: params['client_id']).where(active: params['active'])
    render json: @clients
  end

  def client_or_customer_filter
    @clients = Client.all.where(id: params[:id])
    render json: @clients
  end

  def all_client_or_customer
    @clients = Client.all.where(active: 1).where(indirect: params[:indirect]).order(name: :ASC)
    render json: @clients
  end

  private
    def set_client
      @client = Client.find(params[:id])
    end

    def client_params
      params.permit(
        :indirect,
        :indirect_id,
        :name,
        :address,
        :address2,
        :state,
        :city,
        :zipcode,
        :phone,
        :email,
        :contact_name,
        :status,
        :active)
    end
end
