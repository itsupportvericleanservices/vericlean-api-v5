class WorkOrdersController < ApplicationController
  # before_action :authenticate_user!
  before_action :set_wo, only: [:show, :update, :destroy, :getfiles]

  def index
    @wo = WorkOrder.all

    render json: @wo, include: [:branch, :cleaner, :client]
  end

  def show
    render json: @wo, include: [:branch, :cleaner, :client, invoices: {include: {attachments: {include: {blob: {methods: :service_url}}}}},documents: {include: {attachments: {include: {blob: {methods: :service_url}}}}}, photos: {include: {attachments: {include: {blob: {methods: :service_url}}}}}, signatures: {include: {attachments: {include: {blob: {methods: :service_url}}}}}]
  end

  def create
    @wo = WorkOrder.new(wo_params)

    # Upload Documents
    if params['documents']
      params['documents'].each do |file|
        filename = file['file_name']

        tmp_file = Tempfile.new(filename)
        tmp_file.binmode
        tmp_file.write(Base64.decode64(file['file'].match(/base64,(.*)/)[1]))
        tmp_file.rewind()
        @wo.documents.attach(io: tmp_file, filename: filename, content_type: file['content_type'])
      end

    end

    # Upload Photos
    if params['photos']
      params['photos'].each do |file|
        filename = file['file_name']

        tmp_file = Tempfile.new(filename)
        tmp_file.binmode
        tmp_file.write(Base64.decode64(file['file'].match(/base64,(.*)/)[1]))
        tmp_file.rewind()
        @wo.photos.attach(io: tmp_file, filename: filename, content_type: file['content_type'])
      end

    end

    if @wo.save
      render json: @wo, status: :created, location: @wo
    else
      render json: @wo.errors, status: :unprocessable_entity
    end
  end

  def update
    @user = current_user
    # Upload Documents
    if params['documents']
      params['documents'].each do |file|
        filename = file['file_name']
        tmp_file = Tempfile.new(filename)
        tmp_file.binmode
        tmp_file.write(Base64.decode64(file['file'].match(/base64,(.*)/)[1]))
        tmp_file.rewind()
        @wo.documents.attach(io: tmp_file, filename: filename, content_type: file['content_type'])
      end
    end

    # Upload Photos
    if params['photos']
      params['photos'].each do |file|
        UploadPhotoJob.perform_later(@wo, "#{file['file_name']}", "#{file['content_type']}", "#{file['file']}")
      end
    end

    # Upload Signatures
    if params['signatures']
      params['signatures'].each do |file|
        filename = file['file_name']
        tmp_file = Tempfile.new(filename)
        tmp_file.binmode
        tmp_file.write(Base64.decode64(file['file'].match(/base64,(.*)/)[1]))
        tmp_file.rewind()
        @wo.signatures.attach(io: tmp_file, filename: filename, content_type: file['content_type'])
      end
    end

    # Upload Documents
    if params['invoices']
      params['invoices'].each do |file|
        filename = file['file_name']
        tmp_file = Tempfile.new(filename)
        tmp_file.binmode
        tmp_file.write(Base64.decode64(file['file'].match(/base64,(.*)/)[1]))
        tmp_file.rewind()
        @wo.invoices.attach(io: tmp_file, filename: filename, content_type: file['content_type'])
      end
    end

    if @wo.update(wo_params)
      if wo_params['status'] == '6'
        # binding.pry
        @dispatcher = User.find(wo_params['user_id'])
        @client = Client.find(wo_params['client_id'])
        @resp = WorkOrderMailer.wo_update(wo_params['internal_id'],@client['name'], @client['address'],wo_params['request_contact'], wo_params['request_contact_email'],'The work order has been started.').deliver_now
        @wo.emails << Email.new({
          user_id: @user['id'],
          to: wo_params['request_contact_email'],
          subject: 'Vericlean Work order #'+wo_params['internal_id'],
          body: 'The work order has been started.'
          
        })
      elsif wo_params['status'] == '12'

        @dispatcher = User.find(wo_params['user_id'])
        @client = Client.find(wo_params['client_id'])
        # binding.pry
        @resp = WorkOrderMailer.wo_update(wo_params['internal_id'],@client['name'], @client['address'],@dispatcher['first_name'], wo_params['request_contact_email'],'The work order is ready to verify').deliver_now
        @wo.emails << Email.new({
          user_id: @user['id'],
          to: wo_params['request_contact_email'],
          subject: 'Vericlean Work order #'+wo_params['internal_id'],
          body: 'The work order is ready to verify'
          
        })
      end
      render json: @wo
    else
      render json: @wo.errors, status: :unprocessable_entity
    end
  end
  def email
    @user = current_user
    @resp = WorkOrderMailer.wo_update(params['internal_id'],params['client_name'],params['client_address'],params['request_contact'],params['request_contact_email'],params['body']).deliver_now
    @wo = WorkOrder.find(params['id']);
    @wo['status_alt'] = params['status_alt'];
    

    @wo.emails << Email.new({
      user_id: @user['id'],
      to: params['request_contact_email'],
      subject: 'Vericlean Work order #'+params['internal_id'],
      body: params['body']
      
    })
    @wo.save
    render json: @resp
  end

  def destroy
    @wo.destroy
  end

  def search
    search_str = CGI::escape(params[:search_str])
    @wo = WorkOrder.all.where("comments LIKE ?", "%#{search_str}%")

    render json: @wo
  end


  def getbytype
    @wo = WorkOrder.where("type_main = ?", params['type']);

    render json: @wo
  end

  def wo_for_user 

    user = params['usr']
    wos = WorkOrder.all.where(user_id: user)
    render json: wos, include: [:branch, :cleaner, :client]
  end

  def paginate
    if params['filter'].nil?
      @workorders = WorkOrder.paginate(:page => params[:page], :per_page => 50)
    else
      if params['filter_by'] == 'status'
        @workorders = WorkOrder.all.where(status: params['status']).paginate(:page => params[:page], :per_page => 50)
      end

      if params['filter_by'] == 'priority'
        @workorders = WorkOrder.all.where(priority: params['priority']).paginate(:page => params[:page], :per_page => 50)
      end

      if params['filter_by'] == 'main_type'
        @workorders = WorkOrder.all.where(main_type: params['main_type']).paginate(:page => params[:page], :per_page => 50)
      end
      ###############
      if params['filter_by'] == 'client' || params['filter_by'] == 'customer' ||params['filter_by'] == 'customer_for_client'
        @workorders = WorkOrder.all.where(client_id: params['client_id']).paginate(:page => params[:page], :per_page => 50)
      end

      if params['filter_by'] == 'cleaner'
        @workorders = WorkOrder.all.where(cleaner_id: params['cleaner_id']).paginate(:page => params[:page], :per_page => 50)
      end

      if params['filter_by'] == 'supervisor'
        @workorders = WorkOrder.joins(:branch).where('branches.supervisor_id = ?', params['supervisor_id']).select('work_orders.*').paginate(:page => params[:page], :per_page => 50)
        # @workorders = Branch.all.where(supervisor_id: params['supervisor_id']).where(active: 1).paginate(:page => params[:page], :per_page => 50)
      end

      if params['filter_by'] == 'state'
        @workorders = WorkOrder.joins(:branch).where('branches.state = ?', params['state']).select('work_orders.*').paginate(:page => params[:page], :per_page => 50)
      end

      if params['filter_by'] == 'city'
        @workorders = WorkOrder.joins(:branch).where('branches.city = ?', params['city']).select('work_orders.*').paginate(:page => params[:page], :per_page => 50)
      end
      if params['filter_by'] == 'address'
        @workorders = WorkOrder.joins(:branch).where("branches.address like ?","%#{params['address']}%" ).select('work_orders.*').paginate(:page => params[:page], :per_page => 50)
      end
      if params['filter_by'] == 'site_name'
        @workorders = WorkOrder.joins(:branch).where("branches.name like ?","%#{params['site_name']}%" ).select('work_orders.*').paginate(:page => params[:page], :per_page => 50)
      end
      if params['filter_by'] == 'site_code'
        @workorders = WorkOrder.joins(:branch).where("branches.site_code like ?","%#{params['site_code']}%" ).select('work_orders.*').paginate(:page => params[:page], :per_page => 50)
      end
      if params['filter_by'] == 'customer_work_order'
        @workorders = WorkOrder.all.where("internal_id  like ?","%#{params['customer_work_order']}%" ).paginate(:page => params[:page], :per_page => 50)
      end
    end

    render json: [@workorders, :page => params[:page]], include: [:branch, :cleaner, :client]
  end

  def getfiles
    @url_arr = []
    @wo.images.each do |img|
      @url_arr.push(Rails.application.routes.url_helpers.rails_blob_url(img, only_path: true))
    end

    render json: @url_arr
  end
  private
    def set_wo
      @wo = WorkOrder.find(params[:id])
    end

    def wo_params
      params.require(:work_order).permit(
        :ticket_id,
        :internal_id,
        :extra_id,
        :user_id,
        :branch_id,
        :cleaner_id,
        :client_id,
        :requested_date,
        :due_date,
        :site_contact,
        :site_phone,
        :request_contact,
        :request_contact_email,
        :request_phone,
        :task_name,
        :task_description,
        :status,
        :main_type,
        :priority,
        :fee,
        :scheduled_date,
        :started_date,
        :finished_date,
        :started_lat,
        :started_lng,
        :finished_lat,
        :finished_lng,
        :signature,
        :client_type,
        :comments,
        :instructions,
        images:[:image],
        files:[],
        documents:[],
        photos:[],
        invoices: []
      )
    end
end
