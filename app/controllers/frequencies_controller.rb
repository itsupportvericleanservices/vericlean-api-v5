class FrequenciesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_frequency, only: [:show, :update, :destroy]

  def index
    @frequencies = Frequency.all

    render json: @frequencies
  end

  def show
    render json: @frequency
  end

  def create
    @frequency = Frequency.new(frequency_params)

    if @frequency.save
      render json: @frequency, status: :created
    else
      render json: @frequency.errors, status: :unprocessable_entity
    end
  end

  def update
    if @frequency.update(frequency_params)
      render json: @frequency
    else
      render json: @frequency.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @frequency.destroy
      render json: true
    else
      render json: false
    end
  end

  private
    def set_frequency
      @frequency = Frequency.find(params[:id])
    end

    def frequency_params
      params.permit(
        :name, 
        :days)
    end
end
