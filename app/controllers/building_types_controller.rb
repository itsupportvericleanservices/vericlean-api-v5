class BuildingTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_building_type, only: [:show, :update, :destroy]

  def index
    render json: BuildingType.where.not('deleted')
  end

  def show
    render json: @building_type
  end

  def create
    @building_type = BuildingType.create(building_type_params)

    if @building_type.save
      render json: @building_type, status: :created
    else
      render json: @building_type.errors, status: :unprocessable_entity
    end
  end

  def update
    if @building_type.update(building_type_params)
      render json: @building_type, status: :created
    else
      render json: @building_type.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @building_type.update(deleted: 1)
      render json: true
    else
      render json: false
    end
  end

  private
    def set_building_type
      @building_type = BuildingType.find(params[:id])
    end

    def building_type_params
      params.permit(:name)
    end
end
