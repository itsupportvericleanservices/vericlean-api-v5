class WorkOrderMailer < ApplicationMailer
  default from: 'info@vericleanservices.com'
 
  def wo_update(internal_id,client_name,client_address,site_contact,site_contact_email, body)
    @to = site_contact_email
    @internal_id = internal_id
    @site_contact = site_contact
   	@client_name = client_name
    @client_address = client_address
    @body = body
    mail(to: @to, subject: 'Vericlean Work order #'+@internal_id)
  end

  def wo_dispatcher(internal_id,client_name,client_address,site_contact,site_contact_email)
    @to = site_contact_email
    @internal_id = internal_id
    @site_contact = site_contact
   	@client_name = client_name
    @client_address = client_address
   
    mail(to: @to, subject: 'Vericlean Work order #'+@internal_id)
  end
end
